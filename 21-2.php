<!DOCTYPE html>
<html  lang="zh-Hant-TW">
    <head>
        <title>319鄉鎮</title>
        <!-- ======== META TAGS ======== -->
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <!-- ======== FAVICONS ======== -->
        <link rel="icon" href="favicon.ico">
        <link rel="apple-touch-icon" href="favicon.png">
        <!-- ======== STYLESHEETS ======== -->
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/typography.css">
        <link rel="stylesheet" href="css/fontawesome.css">
        <link rel="stylesheet" href="css/popup.css">
        <link rel="stylesheet" href="css/owlslider.css">
        <link rel="stylesheet" href="css/style.css">
        <link id="switch_style" rel="stylesheet" href="demo/main-color/blue.css">
        <!-- Slidebars CSS -->
        <link rel="stylesheet" href="3dParty/slidebars/slidebars.min.css">
        <!-- ======== RESPONSIVE ======== -->
        <link rel="stylesheet" media="(max-width:479px)" href="css/responsive-small.css">
        <link rel="stylesheet" media="(min-width:480px) and (max-width:768px)" href="css/responsive-0.css">
        <link rel="stylesheet" media="(min-width:769px) and (max-width:992px)" href="css/responsive-768.css">
        <link rel="stylesheet" media="(min-width:993px) and (max-width:1200px)" href="css/responsive-992.css">
        <link rel="stylesheet" media="(min-width:1201px)" href="css/responsive-1200.css">
        
        <!-- Slidebars CSS -->
        <link rel="stylesheet" href="3dParty/owl-carousel/owl.carousel.css">
        <link rel="stylesheet" href="3dParty/owl-carousel/owl.theme.css">

        <!-- MyWeather CSS (needed) -->
        <link rel="stylesheet" type="text/css" href="3dParty/MyWeather/css/MyWeather.css">
        <link rel="stylesheet" type="text/css" href="3dParty/bootstrap/css/zbootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="3dParty/bootstrap/css/bootstrap-theme.min.css">
        <link rel="stylesheet" type="text/css" href="css/zonestyle.css">



        <!-- ======== GOOGLE FONTS ======== -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Titillium+Web:400,300,300italic,400italic,700,700italic,600italic,600">
        <link href='https://fonts.googleapis.com/css?family=PT+Sans:400italic,400|Raleway:200italic,300,300italic|Oxygen:300' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Ubuntu:400,300,300italic' rel='stylesheet' type='text/css'>
        <!-- ======== GOOGLE ANALYTICS ======== -->
    <style>
    .owl-carousel{ display:block !important}

    #owl-demo .item img{
        display: block;
        width: 100%;
        height: auto;
    }


    </style>
    </head>
    <body>
        
        <!-- ======== WRAPPER ======== -->
        <div id="wrapper " class="wide">
        <div id="sb-site">
            <!-- ======== BANNER ======== -->
            <div class="top-banner728x90 text_center aligncenter bg_grey">
				<script type='text/javascript'>
                
                  var googletag = googletag || {};
                
                  googletag.cmd = googletag.cmd || [];
                
                  (function() {
                
                    var gads = document.createElement('script');
                
                    gads.async = true;
                
                    gads.type = 'text/javascript';
                
                    var useSSL = 'https:' == document.location.protocol;
                
                    gads.src = (useSSL ? 'https:' : 'http:') +
                
                      '//www.googletagservices.com/tag/js/gpt.js';
                
                    var node = document.getElementsByTagName('script')[0];
                
                    node.parentNode.insertBefore(gads, node);
                
                  })();
                
                </script>
                <script type='text/javascript'>
                
                  googletag.cmd.push(function() {
                
                    googletag.defineSlot('/47573522/travel_down_728x90', [728, 90], 'div-gpt-ad-1447638964347-0').addService(googletag.pubads());
                
                    googletag.pubads().enableSingleRequest();
                
                    googletag.enableServices();
                
                  });
                
                </script>            
                <a href="#" target="_blank">
                <!-- /47573522/travel_down_728x90 -->
                
                <div id='div-gpt-ad-1447638964347-0' style='height:90px; width:728px; margin:0 auto;'>
                
                <script type='text/javascript'>
                
                googletag.cmd.push(function() { googletag.display('div-gpt-ad-1447638964347-0'); });
                
                </script>
                
                </div>
                </a>
            </div>
            <!-- ======== HEADER ======== -->
            <?php include("header_menu.php");?>
            
            <!-- ======== Slider CONTAINER ======== -->
            <div class="fullwidth bg_grey">
                <div class="container">
                    <ul class="breadcrumb">
                      <li><a href="#">首頁</a></li>
                      <li><a href="#">單元名稱</a></li>
                      <li>頁面名稱</li>
                    </ul> 
                </div> 
            </div>
            
            
            <!-- ======== SECTION ======== -->
            <section id="page_wrapper">
                <div class="container">
                    <div class="row">
                        <!-- ======== MAIN CONTENT ======== -->
                        <div class="col col-sm-12 main_content">
                            <div class="col-sm-5">
                            	<img src="img-main/postcard01.jpg" width="100%"> 
                            </div>
                            <div class="col-sm-7">
                            	<div class="z_titleh5"><a href="#">找尋最棒的原始風景草原</a></div>
                                <div class="z_contentfont">歡迎加入徵笑聯盟，提供與分享你對台灣想表答的影像或文字，把你的分享讓更多人了解。
歡迎加入徵笑聯盟，提供與歡迎加入徵笑聯盟，提供與分享你對台灣想表答的影像或文字，
把你的分享讓更多人了解。歡迎加入徵笑聯盟，提供與歡迎加入徵笑聯盟，提供與分享你對
台灣想表答的影像或文字，把你的分享讓更多人了解。歡迎加入徵笑聯盟，提供與歡迎加入
徵笑聯盟，提供與分享你對台灣想表答的影像或文字，把你的分享讓更多人了解。歡迎加入
徵笑聯盟，提供與歡迎加入徵草原</div>
                                <div class="z_titleh5 z_margintop30"><a href="#">找尋最棒的原始風景草原</a></div>
                                <div class="z_contentfont">歡迎加入徵笑聯盟，提供與分享你對台灣想表答的影像或文字，把你的分享讓更多人了解。
歡迎加入徵笑聯盟，提供與歡迎加入徵笑聯盟，提供與分享你對台灣想表答的影像或文字，
把你的分享讓更多人了解。歡迎加入徵笑聯盟，提供與歡迎加入徵笑聯盟，提供與分享你對
台灣想表答的影像或文字，把你的分享讓更多人了解。歡迎加入徵笑聯盟，提供與歡迎加入
徵笑聯盟，提供與分享你對台灣想表答的影像或文字，把你的分享讓更多人了解。歡迎加入
徵笑聯盟，提供與歡迎加入徵草原</div>
                                <div class="z_titleh5 z_margintop30"><a href="#">找尋最棒的原始風景草原</a></div>
                                <div class="z_contentfont">歡迎加入徵笑聯盟，提供與分享你對台灣想表答的影像或文字，把你的分享讓更多人了解。
歡迎加入徵笑聯盟，提供與歡迎加入徵笑聯盟，提供與分享你對台灣想表答的影像或文字，
把你的分享讓更多人了解。歡迎加入徵笑聯盟，提供與歡迎加入徵笑聯盟，提供與分享你對
台灣想表答的影像或文字，把你的分享讓更多人了解。歡迎加入徵笑聯盟，提供與歡迎加入
徵笑聯盟，提供與分享你對台灣想表答的影像或文字，把你的分享讓更多人了解。歡迎加入
徵笑聯盟，提供與歡迎加入徵草原</div>
                               
                               
                              
                            </div> 
                            <div class="clearfix"></div> 
                            <div class="z_margintop10 z_border_cc">
                               	    	<img src="img-main/banner728x90.jpg" class="visible-lg visible-sm visible-md"> 
                                        <img src="demo/banners/320X100.jpg" class="visible-xs">
                            </div> 
                            <div class="clearfix"></div> 
                            <div class="col-sm-5 z_margintop30">
                            	<img src="img-main/postcard01.jpg" width="100%"> 
                                <div class="clearfix"></div>
                                <div class="row z_margintop30">
                                <div class="col-sm-6">
                                <img src="img-main/postcard03.jpg" width="100%"> 
                                </div>
                                <div class="col-sm-6">
                                <img src="img-main/post-demo.jpg" width="100%"> 
                                </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="col-sm-7 z_margintop30">
                            	<div class="z_titleh5"><a href="#">找尋最棒的原始風景草原</a></div>
                                <div class="z_contentfont">歡迎加入徵笑聯盟，提供與分享你對台灣想表答的影像或文字，把你的分享讓更多人了解。
歡迎加入徵笑聯盟，提供與歡迎加入徵笑聯盟，提供與分享你對台灣想表答的影像或文字，
把你的分享讓更多人了解。歡迎加入徵笑聯盟，提供與歡迎加入徵笑聯盟，提供與分享你對
台灣想表答的影像或文字，把你的分享讓更多人了解。歡迎加入徵笑聯盟，提供與歡迎加入
徵笑聯盟，提供與分享你對台灣想表答的影像或文字，把你的分享讓更多人了解。歡迎加入
徵笑聯盟，提供與歡迎加入徵草原</div>
                                <div class="z_titleh5 z_margintop30"><a href="#">找尋最棒的原始風景草原</a></div>
                                <div class="z_contentfont">歡迎加入徵笑聯盟，提供與分享你對台灣想表答的影像或文字，把你的分享讓更多人了解。
歡迎加入徵笑聯盟，提供與歡迎加入徵笑聯盟，提供與分享你對台灣想表答的影像或文字，
把你的分享讓更多人了解。歡迎加入徵笑聯盟，提供與歡迎加入徵笑聯盟，提供與分享你對
台灣想表答的影像或文字，把你的分享讓更多人了解。歡迎加入徵笑聯盟，提供與歡迎加入
徵笑聯盟，提供與分享你對台灣想表答的影像或文字，把你的分享讓更多人了解。歡迎加入
徵笑聯盟，提供與歡迎加入徵草原</div>
                                <div class="z_titleh5 z_margintop30"><a href="#">找尋最棒的原始風景草原</a></div>
                                <div class="z_contentfont">歡迎加入徵笑聯盟，提供與分享你對台灣想表答的影像或文字，把你的分享讓更多人了解。
歡迎加入徵笑聯盟，提供與歡迎加入徵笑聯盟，提供與分享你對台灣想表答的影像或文字，
把你的分享讓更多人了解。歡迎加入徵笑聯盟，提供與歡迎加入徵笑聯盟，提供與分享你對
台灣想表答的影像或文字，把你的分享讓更多人了解。歡迎加入徵笑聯盟，提供與歡迎加入
徵笑聯盟，提供與分享你對台灣想表答的影像或文字，把你的分享讓更多人了解。歡迎加入
徵笑聯盟，提供與歡迎加入徵草原</div>
                               
                               
                              
                            </div>  
                             
                            <div class="clearfix"></div>
                            
                            <div class="z_margintop10 z_border_cc">
                               	    	<img src="img-main/banner728x90.jpg" class="visible-lg visible-sm visible-md"> 
                                        <img src="demo/banners/320X100.jpg" class="visible-xs">
                            </div> 
                            <div class="clearfix"></div>
                            <div style="height:50px"></div>    
                        </div>
                        <!-- ======== 12of12 ======== -->
                    </div>
                </div>
                <!-- ======== container ======== -->
            </section>
            <!-- ======== FOOTER ======== -->
            <?php include("footer.php");?>

</div>
        </div>
        
        <!--leftmenu-->
    <div class="sb-slidebar sb-left sb-style-overlay">
        <!-- Main Navigation -->
        <?php include("left_menu.php");?>
        <!-- /Main Navigation -->
    </div>
        <!--righttmenu-->
		<div class="sb-slidebar sb-right sb-style-overlay">
          <div class="col col_12_of_12 sidebar">
                              <!-- ======== WIDGET - BANNER 300X100 ======== -->
                              <div class="widget banner300x100 margin-bottom-30">
                                  <div class="widget_title">
                                  <img src="img-main/title-special.png" alt=""/>
                                  </div>
                                <div class="banner margin-bottom-30">
                                    <a href="http://smiletaiwan.cw.com.tw/12summer" target="_blank"><img src="img-main/sp_01.jpg" alt=""></a>
                                    <p>追日。追風。追海的旅行</p>
                                </div>
                                <div class="banner margin-bottom-30">
                                    <a href="http://smiletaiwan.cw.com.tw/matsufun" target="_blank"><img src="img-main/sp_02.jpg" alt=""></a>
                                    <p>杖起舵兒往前滑！馬祖，等你</p>
                                </div>
                                <div class="banner margin-bottom-30">
                                    <a href="http://smiletaiwan.cw.com.tw/native" target="_blank"><img src="img-main/sp_03.jpg" alt=""></a>
                                    <p>傾聽部落。你有東西留在我這</p>
                                </div>
                              </div>
                          
                              <!-- ======== WIDGET - 駐站旅人 ======== -->
                              <div class="widget widget_socialize margin-bottom-20">
                                  <div class="widget_title no-border-bottom">
                                  <img src="img-main/title-traveler.png" alt=""/> 
                                  </div>
                                  <div class="row">
                                      <div class="col_4_of_12 alignleft">
                                      <div class="writer-single">
                                                      <img src="img-main/person_pic_default.jpg" alt="">
                                                      <p>作者名稱</p>
                                                  </div>
                                      </div>
                                      <div class="col_4_of_12 alignleft">
                                      <div class="writer-single">
                                                      <img src="img-main/person_pic_default.jpg" alt="">
                                                      <p>作者名稱</p>
                                                  </div>
                                      </div>
                                      <div class="col_4_of_12 alignleft">
                                      <div class="writer-single">
                                                      <img src="img-main/person_pic_default.jpg" alt="">
                                                      <p>作者名稱</p>
                                                  </div>
                                      </div>
                                      <div class="col_4_of_12 alignleft">
                                      <div class="writer-single">
                                                      <img src="img-main/person_pic_default.jpg" alt="">
                                                      <p>作者名稱</p>
                                                  </div>
                                      </div>
                                      <div class="col_4_of_12 alignleft">
                                      <div class="writer-single">
                                                      <img src="img-main/person_pic_default.jpg" alt="">
                                                      <p>作者名稱</p>
                                                  </div>
                                      </div>
                                      <div class="col_4_of_12 alignleft">
                                      <div class="writer-single">
                                                      <img src="img-main/person_pic_default.jpg" alt="">
                                                      <p>作者名稱</p>
                                                  </div>
                                      </div>
                                  </div>
                              </div>
                              <!-- ======== WIDGET - BANNER 300X250 ======== -->
                              <div class="widget widget_banner_300x250 margin-bottom-20">
                                  <div class="banner">
									<script type='text/javascript'>
                                    
                                      var googletag = googletag || {};
                                    
                                      googletag.cmd = googletag.cmd || [];
                                    
                                      (function() {
                                    
                                        var gads = document.createElement('script');
                                    
                                        gads.async = true;
                                    
                                        gads.type = 'text/javascript';
                                    
                                        var useSSL = 'https:' == document.location.protocol;
                                    
                                        gads.src = (useSSL ? 'https:' : 'http:') +
                                    
                                          '//www.googletagservices.com/tag/js/gpt.js';
                                    
                                        var node = document.getElementsByTagName('script')[0];
                                    
                                        node.parentNode.insertBefore(gads, node);
                                    
                                      })();
                                    
                                    </script>
                                    <script type='text/javascript'>
                                    
                                      googletag.cmd.push(function() {
                                    
                                        googletag.defineSlot('/47573522/article_right_300x250(u)', [300, 250], 'div-gpt-ad-1447638783144-0').addService(googletag.pubads());
                                    
                                        googletag.pubads().enableSingleRequest();
                                    
                                        googletag.enableServices();
                                    
                                      });
                                    
                                    </script>                                
                                    <a href="#" target="_blank">
                                    <!-- /47573522/article_right_300x250(u) -->
                                    <div id='div-gpt-ad-1447638783144-0' style='height:250px; width:300px;'>
                                    
                                    <script type='text/javascript'>
                                    
                                    googletag.cmd.push(function() { googletag.display('div-gpt-ad-1447638783144-0'); });
                                    
                                    </script>
                                    
                                    </div>
                                    </a>
                                  </div>
                              </div>
                              <!-- ======== WIDGET - LATEST POSTS ======== -->
                              <div class="widget widget_latest_posts margin-bottom-20">
                                  <div class="widget_title no-border-bottom">
                                  <img src="img-main/title_top.png" alt=""/> 
                                  </div>
                                  <div class="item">
                                      <div class="item_header">
                                          <i class="fa fa-3x fa-angle-right text_grey"></i>
                                      </div>
                                      <div class="item_wrapper">
                                          <div class="item_content">
                                            <h4><a #href="#" target="_blank">【新竹 旅遊】客家湯圓節健走。傳統技藝新玩法</a></h4>
                                             <p>哪哪麻這天來到竹東火車站參加湯圓節的活動, 話說竹東每年都會辦一次湯圓節, 有園遊會賣當地特色產品 ...</p>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="item">
                                      <div class="item_header">
                                          <i class="fa fa-3x fa-angle-right text_grey"></i>
                                      </div>
                                      <div class="item_wrapper">
                                          <div class="item_content">
                                            <h4><a #href="#" target="_blank">當老師問我……</a></h4>
                                             <p>當老師問我： 「耶誕節都怎樣慶祝？」當我的外籍老師問我時，直覺地脫口而出，「這個啊，我們都過農曆年的。」 老師很有興致，...</p>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="item">
                                      <div class="item_header">
                                          <i class="fa fa-3x fa-angle-right text_grey"></i>
                                      </div>
                                      <div class="item_wrapper">
                                          <div class="item_content">
                                            <h4><a #href="#" target="_blank">台中大雪山．步道散步吸收芬多精 & 雪山神木 & 天</a></h4>
                                             <p>炎熱的夏天，是不是很想上山消暑呢？鳥夫人：不是~~~~ 呵呵，因為鳥夫人喜歡大海不喜歡山，但鳥先生跟鳥夫人相反，所以兩個人...</p>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                              <!-- ======== WIDGET - BANNER 300X250 ======== -->
                              <div class="widget widget_banner_300x250">
                                  <div class="banner margin-bottom-30">
								  <script type='text/javascript'>
                                  
                                    var googletag = googletag || {};
                                  
                                    googletag.cmd = googletag.cmd || [];
                                  
                                    (function() {
                                  
                                      var gads = document.createElement('script');
                                  
                                      gads.async = true;
                                  
                                      gads.type = 'text/javascript';
                                  
                                      var useSSL = 'https:' == document.location.protocol;
                                  
                                      gads.src = (useSSL ? 'https:' : 'http:') +
                                  
                                        '//www.googletagservices.com/tag/js/gpt.js';
                                  
                                      var node = document.getElementsByTagName('script')[0];
                                  
                                      node.parentNode.insertBefore(gads, node);
                                  
                                    })();
                                  
                                  </script>
                                  <script type='text/javascript'>
                                  
                                    googletag.cmd.push(function() {
                                  
                                      googletag.defineSlot('/47573522/article_right_300x250(d)', [300, 250], 'div-gpt-ad-1447638832673-0').addService(googletag.pubads());
                                  
                                      googletag.pubads().enableSingleRequest();
                                  
                                      googletag.enableServices();
                                  
                                    });
                                  
                                  </script>
                                  <a href="#" target="_blank">
                                  <!-- /47573522/article_right_300x250(d) -->
                                  <div id='div-gpt-ad-1447638832673-0' style='height:250px; width:300px;'>
                                  
                                  <script type='text/javascript'>
                                  
                                  googletag.cmd.push(function() { googletag.display('div-gpt-ad-1447638832673-0'); });
                                  
                                  </script>
                                  
                                  </div>
                                  </a>
                                  </div>
                              </div>
                          </div>
        </div>
        
        
        
        <!-- ======== JAVASCRIPTS ======== -->
        <script src="js/jquery.min.js"></script>
        <script src="js/jquery.ui.min.js"></script>
        <script src="js/jquery.fitvids.js"></script>
        <script src="js/jquery.owlcarousel.min.js"></script>
        <script src="js/jquery.magnific.popup.min.js"></script>
        <script src="js/jquery.parallax.min.js"></script>
        <script src="js/jquery.smooth.scroll.js"></script>
        <script src="js/jquery.init.js"></script>
         <script src="sys_plugin/oki_zippicker/oki_zipcode_mem.js"></script>
  <!-- MyWeather JS (needed) -->
  <script src="3dParty/MyWeather/js/MyWeather.js"></script>
  <script src="3dParty/bootstrap/js/bootstrap.minzone.js"></script>
       
        
<!-- owl-carousel -->
    <script>
    $(document).ready(function() {
      $("#owl-demo").owlCarousel({
          navigation : false, // Show next and prev buttons
          slideSpeed : 300,
          paginationSpeed : 400,
          singleItem:true,
      autoPlay : true
     
          // "singleItem:true" is a shortcut for:
          // items : 1, 
          // itemsDesktop : false,
          // itemsDesktopSmall : false,
          // itemsTablet: false,
          // itemsMobile : false
      });
	  $('#myTab a').click(function(e) {
                e.preventDefault()
                $(this).tab('show')
            })
    });
  $('#t_owl_slider').owlCarousel({
          navigation : false, // Show next and prev buttons
          slideSpeed : 300,
          paginationSpeed : 400,
          singleItem:true,
      autoPlay : true,
})

  $('.shop_owl_slider').owlCarousel({
          navigation : true, // Show next and prev buttons
          pagination : false,
          slideSpeed : 300,
          paginationSpeed : 400,
          autoPlay : true,
loop:true,
    margin:10,
    items : 3, 
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        468:{
            items:2,
            nav:false
        },
        989:{
            items:3,
            nav:true,
            loop:false
        }
    }
})
  $('.postcard_owl_slider').owlCarousel({
          navigation : true, // Show next and prev buttons
          pagination : false,
          slideSpeed : 300,
          paginationSpeed : 400,
          autoPlay : true,
loop:true,
    margin:10,
    items : 3, 
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        468:{
            items:2,
            nav:false
        },
        989:{
            items:3,
            nav:true,
            loop:false
        }
    }
})
$('.postcard_owl_slider2').owlCarousel({
          navigation : true, // Show next and prev buttons
          pagination : false,
          slideSpeed : 300,
          paginationSpeed : 400,
          autoPlay : true,
loop:true,
    margin:10,
    items : 2, 
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        468:{
            items:2,
            nav:false
        },
        989:{
            items:3,
            nav:true,
            loop:false
        }
    }
})
    </script>
    
<!-- Slidebars -->
<script src="3dParty/slidebars/slidebars.min.js"></script>
<script>
    (function($) {
        $(document).ready(function() {
            $.slidebars();
      
      $("#schbtn").on("click",function(){ $("#schform").submit() });
      
        });
    }) (jQuery);
</script>

<!-- weather -->
<script type="text/javascript">

 $.MyWeather({
           elementid: "#localinfo",
           position: "right",
           showpopup: true,
           showinfo: true,
           temperature: "c",
           closeicon: false,
           showicon: true,
           showtemperature: true,
           showlocation: true,
           showip: false,
           size: 80,
           iconcolor: "#ffffff",
           fontcolor: "#ffffff",
},
function(City,Country, IP, Latitude, Longitude, Temperature){
// This callback function is called when we get those values. The callback is optional too.
}); 


</script> 

    </body>
</html>
<script>