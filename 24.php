<!DOCTYPE html>
<html lang="en-US">
    <head>
        <title>旅人專欄</title>
        <!-- ======== META TAGS ======== -->
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <!-- ======== FAVICONS ======== -->
        <link rel="icon" href="favicon.ico">
        <link rel="apple-touch-icon" href="favicon.png">
        <!-- ======== STYLESHEETS ======== -->
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/typography.css">
        <link rel="stylesheet" href="css/fontawesome.css">
        <link rel="stylesheet" href="css/popup.css">
        <link rel="stylesheet" href="css/owlslider.css">
        <link rel="stylesheet" href="css/style.css">
        <link id="switch_style" rel="stylesheet" href="demo/main-color/blue.css">
        <!-- Slidebars CSS -->
        <link rel="stylesheet" href="3dParty/slidebars/slidebars.min.css">
        <!-- ======== RESPONSIVE ======== -->
        <link rel="stylesheet"  href="css/responsive-992.css">
        
        <!-- Slidebars CSS -->
        <link rel="stylesheet" href="3dParty/owl-carousel/owl.carousel.css">
        <link rel="stylesheet" href="3dParty/owl-carousel/owl.theme.css">
        <!-- ======== GOOGLE FONTS ======== -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Titillium+Web:400,300,300italic,400italic,700,700italic,600italic,600">
        <link rel="stylesheet" type="text/css" href="3dParty/bootstrap/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="3dParty/bootstrap/css/zbootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="3dParty/bootstrap/css/bootstrap-theme.min.css">
    </head>
    <body>
        
        <!-- ======== WRAPPER ======== -->
        <div id="wrapper" class="wide">
        <div id="sb-site">
            <!-- ======== BANNER ======== -->
            <div class="top-banner728x90 text_center aligncenter bg_grey">
				<script type='text/javascript'>
                
                  var googletag = googletag || {};
                
                  googletag.cmd = googletag.cmd || [];
                
                  (function() {
                
                    var gads = document.createElement('script');
                
                    gads.async = true;
                
                    gads.type = 'text/javascript';
                
                    var useSSL = 'https:' == document.location.protocol;
                
                    gads.src = (useSSL ? 'https:' : 'http:') +
                
                      '//www.googletagservices.com/tag/js/gpt.js';
                
                    var node = document.getElementsByTagName('script')[0];
                
                    node.parentNode.insertBefore(gads, node);
                
                  })();
                
                </script>
                <script type='text/javascript'>
                
                  googletag.cmd.push(function() {
                
                    googletag.defineSlot('/47573522/travel_down_728x90', [728, 90], 'div-gpt-ad-1447638964347-0').addService(googletag.pubads());
                
                    googletag.pubads().enableSingleRequest();
                
                    googletag.enableServices();
                
                  });
                
                </script>            
                <a href="#" target="_blank">
                <!-- /47573522/travel_down_728x90 -->
                
                <div id='div-gpt-ad-1447638964347-0' style='height:90px; width:728px; margin:0 auto;'>
                
                <script type='text/javascript'>
                
                googletag.cmd.push(function() { googletag.display('div-gpt-ad-1447638964347-0'); });
                
                </script>
                
                </div>
                </a>
            </div>
            <!-- ======== HEADER ======== -->
            <?php include("header_menu.php");?>

            <!-- ======== FEATURED BANNER ======== -->
            <div class="featured_banner">
            <img src="demo/parallax-container/11.jpg" alt=""/> 
            </div>
            <!-- ======== BREADCRUMB ======== -->
            <div class="fullwidth bg_grey">
                <div class="container">
                    <ul class="breadcrumb">
                      <li><a href="#">首頁</a></li>
                      <li><a href="#">單元名稱</a></li>
                      <li>頁面名稱</li>
                    </ul> 
                </div> 
            </div>
            <!-- ======== SECTION ======== -->
            <section id="page_wrapper">
                <div class="container">
                    <div class="row">
                        <!-- ======== SIDEBAR ======== -->
                        <div class="col col_4_of_12 sidebar sb_right300 hasborder">
                                <header class="post_header padding_bottom_20 border-bottom">
                                <div class="row item writerphoto margin-center">
                                  <div class="item_header padding_20 text_center">
                                      <img src="demo/avatars/1.jpg" alt="Avatar">
                                  </div>
                                </div>
                                  <div class="item_wrapper">
                                  <div class="item_info padding_10">
                                      <h3 class="author text_left no_paddong_bottom">旅人  彭阿東</h3>
                                      <div class="comment ">來自宜蘭的彭阿東，喜歡在各鄉鎮間漫步遊走，分享我的在地故事、深度旅行 [...]</div>
                                  </div>    
                                  </div>
                                  <div class="clearfix"><a class="btn btn_large pull-right " href="#">編輯</a></div>
                                </header>
                                <?php include('member_btnlist.php')?>
                        </div>
                        <!-- ======== MAIN CONTENT ======== -->
                        <div class="col col_8_of_12 main_content">
                            <!-- ======== SINGLE ARTICLE ======== -->
                            <article class="single_post hasborder">
                                <!-- ======== ARTICLE CONTENT ==== -->
                                
                                <div class="col col_12_of_12">
                                      <div class="post_content">
                                           <div class="title_hot border-bottom padding_bottom_10 margin-bottom-10 padding_top_20 zone_fontsize">
                                            使用空間限制
                                           </div>
                                           <form action="" method="get">
                                           <div class="row">                                           		
                                             <div class="col col_12_of_12"> 
                                                    <p style="font-size:60px;">總計 3GB</p>
                                                    <div class="zone_left_blueline">
                                                        <div class="row">
                                                            <div class="col col_5_of_12">
                                                            尚有 <span style="color:#f60;">1GB</span> 可用空間 已使用 <span style="color:#f60;">2GB</span>
                                                            </div>                                                            
                                                            <div class="col col_7_of_12" style="text-align:right;">
                                                            <a class="btn btn_lm" href="#" style="height:25px; line-height:10px;">額外空間申請</a>
                                                            </div>
                                                        </div>                                                    
                                                    </div> 
                                                    <p class="border-bottom" style="height:1px;">&nbsp;</p>
                                                    <div class="zone_left_blueline">
                                                        <div class="row">
                                                            <div class="col col_12_of_12">
                                                            &nbsp;&nbsp;&nbsp;旅行明信片： 目前已使用 <span style="color:#f60;">1.2GB</span>
                                                                                                            
                                                            </div>
                                                            
                                                        </div>                                                    
                                                    </div>
                                                    <div class="zone_left_blueline">
                                                        <div class="row">
                                                            <div class="col col_12_of_12">
                                                            駐站旅人文章： 目前已使用 <span style="color:#f60;">800 MB</span>
                                                                                                            
                                                            </div>
                                                            
                                                        </div>                                                    
                                                    </div>
                                                    
                                                    
                                             </div>
                                                <!--結 -->
                                                
                                                
                                                                                                
                                           </div>
                                        
                                            <!--結 -->
                                            </form>            
                                      </div>                                	
                                </div>
                                <div class="clearfix"></div>
                            </article>
                            
                            
                            
                            
                            
                        </div>
                    </div>
                </div>
            </section>
            <!-- ======== FOOTER ======== -->
            <?php include("footer.php");?>

        </div>
        </div>
        
        
        
        <!--leftmenu-->
    <div class="sb-slidebar sb-left sb-style-overlay">
        <!-- Main Navigation -->
        <?php include("left_menu.php");?>
        <!-- /Main Navigation -->
    </div>
        <!--righttmenu-->
		<div class="sb-slidebar sb-right sb-style-overlay">
          <div class="col col_12_of_12 sidebar">
                              <!-- ======== WIDGET - BANNER 300X100 ======== -->
                              <div class="widget banner300x100 margin-bottom-30">
                                  <div class="widget_title">
                                  <img src="img-travel/title-special.png" alt=""/>
                                  </div>
                                <div class="banner margin-bottom-30">
                                    <a href="http://smiletaiwan.cw.com.tw/12summer" target="_blank"><img src="img-travel/sp_01.jpg" alt=""></a>
                                    <p>追日。追風。追海的旅行</p>
                                </div>
                                <div class="banner margin-bottom-30">
                                    <a href="http://smiletaiwan.cw.com.tw/matsufun" target="_blank"><img src="img-travel/sp_02.jpg" alt=""></a>
                                    <p>杖起舵兒往前滑！馬祖，等你</p>
                                </div>
                                <div class="banner margin-bottom-30">
                                    <a href="http://smiletaiwan.cw.com.tw/native" target="_blank"><img src="img-travel/sp_03.jpg" alt=""></a>
                                    <p>傾聽部落。你有東西留在我這</p>
                                </div>
                              </div>
                          
                              <!-- ======== WIDGET - 駐站旅人 ======== -->
                              <div class="widget widget_socialize">
                                  <div class="widget_title no-border-bottom">
                                  <img src="img-travel/title-traveler.png" alt=""/> 
                                  </div>
                                  <div class="row">
                                      <div class="col_4_of_12 alignleft">
                                      <div class="writer-single">
                                                      <img src="img-travel/person_pic_default.jpg" alt="">
                                                      <p>作者名稱</p>
                                                  </div>
                                      </div>
                                      <div class="col_4_of_12 alignleft">
                                      <div class="writer-single">
                                                      <img src="img-travel/person_pic_default.jpg" alt="">
                                                      <p>作者名稱</p>
                                                  </div>
                                      </div>
                                      <div class="col_4_of_12 alignleft">
                                      <div class="writer-single">
                                                      <img src="img-travel/person_pic_default.jpg" alt="">
                                                      <p>作者名稱</p>
                                                  </div>
                                      </div>
                                      <div class="col_4_of_12 alignleft">
                                      <div class="writer-single">
                                                      <img src="img-travel/person_pic_default.jpg" alt="">
                                                      <p>作者名稱</p>
                                                  </div>
                                      </div>
                                      <div class="col_4_of_12 alignleft">
                                      <div class="writer-single">
                                                      <img src="img-travel/person_pic_default.jpg" alt="">
                                                      <p>作者名稱</p>
                                                  </div>
                                      </div>
                                      <div class="col_4_of_12 alignleft">
                                      <div class="writer-single">
                                                      <img src="img-travel/person_pic_default.jpg" alt="">
                                                      <p>作者名稱</p>
                                                  </div>
                                      </div>
                                  </div>
                              </div>
                              <!-- ======== WIDGET - BANNER 300X250 ======== -->
                              <div class="widget widget_banner_300x250">
                                  <div class="banner">
									<script type='text/javascript'>
                                    
                                      var googletag = googletag || {};
                                    
                                      googletag.cmd = googletag.cmd || [];
                                    
                                      (function() {
                                    
                                        var gads = document.createElement('script');
                                    
                                        gads.async = true;
                                    
                                        gads.type = 'text/javascript';
                                    
                                        var useSSL = 'https:' == document.location.protocol;
                                    
                                        gads.src = (useSSL ? 'https:' : 'http:') +
                                    
                                          '//www.googletagservices.com/tag/js/gpt.js';
                                    
                                        var node = document.getElementsByTagName('script')[0];
                                    
                                        node.parentNode.insertBefore(gads, node);
                                    
                                      })();
                                    
                                    </script>
                                    <script type='text/javascript'>
                                    
                                      googletag.cmd.push(function() {
                                    
                                        googletag.defineSlot('/47573522/article_right_300x250(u)', [300, 250], 'div-gpt-ad-1447638783144-0').addService(googletag.pubads());
                                    
                                        googletag.pubads().enableSingleRequest();
                                    
                                        googletag.enableServices();
                                    
                                      });
                                    
                                    </script>                                
                                    <a href="#" target="_blank">
                                    <!-- /47573522/article_right_300x250(u) -->
                                    <div id='div-gpt-ad-1447638783144-0' style='height:250px; width:300px;'>
                                    
                                    <script type='text/javascript'>
                                    
                                    googletag.cmd.push(function() { googletag.display('div-gpt-ad-1447638783144-0'); });
                                    
                                    </script>
                                    
                                    </div>
                                    </a>
                                  </div>
                              </div>
                              <!-- ======== WIDGET - LATEST POSTS ======== -->
                              <div class="widget widget_latest_posts">
                                  <div class="widget_title no-border-bottom">
                                  <img src="img-travel/title_top.png" alt=""/> 
                                  </div>
                                  <div class="item">
                                      <div class="item_header">
                                          <i class="fa fa-3x fa-angle-right text_grey"></i>
                                      </div>
                                      <div class="item_wrapper">
                                          <div class="item_content">
                                            <h4><a #href="http://319travel.kay.tw/?c=post&uid=12" target="_blank">【新竹 旅遊】客家湯圓節健走。傳統技藝新玩法</a></h4>
                                             <p>哪哪麻這天來到竹東火車站參加湯圓節的活動, 話說竹東每年都會辦一次湯圓節, 有園遊會賣當地特色產品 ...</p>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="item">
                                      <div class="item_header">
                                          <i class="fa fa-3x fa-angle-right text_grey"></i>
                                      </div>
                                      <div class="item_wrapper">
                                          <div class="item_content">
                                            <h4><a #href="http://319travel.kay.tw/?c=post&uid=18" target="_blank">當老師問我……</a></h4>
                                             <p>當老師問我： 「耶誕節都怎樣慶祝？」當我的外籍老師問我時，直覺地脫口而出，「這個啊，我們都過農曆年的。」 老師很有興致，...</p>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="item">
                                      <div class="item_header">
                                          <i class="fa fa-3x fa-angle-right text_grey"></i>
                                      </div>
                                      <div class="item_wrapper">
                                          <div class="item_content">
                                            <h4><a #href="http://319travel.kay.tw/?c=post&uid=14" target="_blank">台中大雪山．步道散步吸收芬多精 & 雪山神木 & 天</a></h4>
                                             <p>炎熱的夏天，是不是很想上山消暑呢？鳥夫人：不是~~~~ 呵呵，因為鳥夫人喜歡大海不喜歡山，但鳥先生跟鳥夫人相反，所以兩個人...</p>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                              <!-- ======== WIDGET - BANNER 300X250 ======== -->
                              <div class="widget widget_banner_300x250">
                                  <div class="banner">
								  <script type='text/javascript'>
                                  
                                    var googletag = googletag || {};
                                  
                                    googletag.cmd = googletag.cmd || [];
                                  
                                    (function() {
                                  
                                      var gads = document.createElement('script');
                                  
                                      gads.async = true;
                                  
                                      gads.type = 'text/javascript';
                                  
                                      var useSSL = 'https:' == document.location.protocol;
                                  
                                      gads.src = (useSSL ? 'https:' : 'http:') +
                                  
                                        '//www.googletagservices.com/tag/js/gpt.js';
                                  
                                      var node = document.getElementsByTagName('script')[0];
                                  
                                      node.parentNode.insertBefore(gads, node);
                                  
                                    })();
                                  
                                  </script>
                                  <script type='text/javascript'>
                                  
                                    googletag.cmd.push(function() {
                                  
                                      googletag.defineSlot('/47573522/article_right_300x250(d)', [300, 250], 'div-gpt-ad-1447638832673-0').addService(googletag.pubads());
                                  
                                      googletag.pubads().enableSingleRequest();
                                  
                                      googletag.enableServices();
                                  
                                    });
                                  
                                  </script>
                                  <a href="#" target="_blank">
                                  <!-- /47573522/article_right_300x250(d) -->
                                  <div id='div-gpt-ad-1447638832673-0' style='height:250px; width:300px;'>
                                  
                                  <script type='text/javascript'>
                                  
                                  googletag.cmd.push(function() { googletag.display('div-gpt-ad-1447638832673-0'); });
                                  
                                  </script>
                                  
                                  </div>
                                  </a>
                                  </div>
                              </div>
                          </div>
        </div>
        
        
        
        <!-- ======== JAVASCRIPTS ======== -->
        <script src="js/jquery.min.js"></script>
        <script src="js/jquery.ui.min.js"></script>
        <script src="js/jquery.fitvids.js"></script>
        <script src="js/jquery.owlcarousel.min.js"></script>
        <script src="js/jquery.magnific.popup.min.js"></script>
        <script src="js/jquery.parallax.min.js"></script>
        <script src="js/jquery.smooth.scroll.js"></script>
        <script src="js/jquery.init.js"></script>
        
<!-- owl-carousel -->
<script src="3dParty/owl-carousel/owl.carousel.js"></script>
    <script>
    $(document).ready(function() {
      $("#owl-postcard").owlCarousel({
          navigation : false, // Show next and prev buttons
          slideSpeed : 300,
          paginationSpeed : 400,
          singleItem:true,
		  autoPlay : true
     
          // "singleItem:true" is a shortcut for:
          // items : 1, 
          // itemsDesktop : false,
          // itemsDesktopSmall : false,
          // itemsTablet: false,
          // itemsMobile : false
      });
    });
    </script>
    <script>
    $(document).ready(function() {
      $("#owl-post").owlCarousel({
          navigation : false, // Show next and prev buttons
          slideSpeed : 300,
          paginationSpeed : 400,
          singleItem:true,
		  autoPlay : true
     
          // "singleItem:true" is a shortcut for:
          // items : 1, 
          // itemsDesktop : false,
          // itemsDesktopSmall : false,
          // itemsTablet: false,
          // itemsMobile : false
      });
    });
    </script>
    
<!-- Slidebars -->
<script src="3dParty/slidebars/slidebars.min.js"></script>
<script>
    (function($) {
        $(document).ready(function() {
            $.slidebars();
        });
		
					$(".post_content img").removeAttr("style","").removeAttr("height","").removeAttr("width","");						
					$(".post_content img").css("max-width","100%!important");
		
    }) (jQuery);
	
	
	        
	
</script>
        
    </body>
</html>
