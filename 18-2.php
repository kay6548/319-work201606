<!DOCTYPE html>
<html  lang="zh-Hant-TW">
    <head>
        <title>319鄉鎮</title>
        <!-- ======== META TAGS ======== -->
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <!-- ======== FAVICONS ======== -->
        <link rel="icon" href="favicon.ico">
        <link rel="apple-touch-icon" href="favicon.png">
        <!-- ======== STYLESHEETS ======== -->
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/typography.css">
        <link rel="stylesheet" href="css/fontawesome.css">
        <link rel="stylesheet" href="css/popup.css">
        <link rel="stylesheet" href="css/owlslider.css">
        <link rel="stylesheet" href="css/style.css">
        <link id="switch_style" rel="stylesheet" href="demo/main-color/blue.css">
        <!-- Slidebars CSS -->
        <link rel="stylesheet" href="3dParty/slidebars/slidebars.min.css">
        <!-- ======== RESPONSIVE ======== -->
        <link rel="stylesheet" media="(max-width:479px)" href="css/responsive-small.css">
        <link rel="stylesheet" media="(min-width:480px) and (max-width:768px)" href="css/responsive-0.css">
        <link rel="stylesheet" media="(min-width:769px) and (max-width:992px)" href="css/responsive-768.css">
        <link rel="stylesheet" media="(min-width:993px) and (max-width:1200px)" href="css/responsive-992.css">
        <link rel="stylesheet" media="(min-width:1201px)" href="css/responsive-1200.css">
        
        <!-- Slidebars CSS -->
        <link rel="stylesheet" href="3dParty/owl-carousel/owl.carousel.css">
        <link rel="stylesheet" href="3dParty/owl-carousel/owl.theme.css">

        <!-- MyWeather CSS (needed) -->
        <link rel="stylesheet" type="text/css" href="3dParty/MyWeather/css/MyWeather.css">
        <link rel="stylesheet" type="text/css" href="3dParty/bootstrap/css/zbootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="3dParty/bootstrap/css/bootstrap-theme.min.css">
        <link rel="stylesheet" type="text/css" href="css/zonestyle.css">



        <!-- ======== GOOGLE FONTS ======== -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Titillium+Web:400,300,300italic,400italic,700,700italic,600italic,600">
        <link href='https://fonts.googleapis.com/css?family=PT+Sans:400italic,400|Raleway:200italic,300,300italic|Oxygen:300' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Ubuntu:400,300,300italic' rel='stylesheet' type='text/css'>
        <!-- ======== GOOGLE ANALYTICS ======== -->
    <style>
    .owl-carousel{ display:block !important}

    #owl-demo .item img{
        display: block;
        width: 100%;
        height: auto;
    }


    </style>
    </head>
    <body>
        
        <!-- ======== WRAPPER ======== -->
        <div id="wrapper " class="wide">
        <div id="sb-site">
            <!-- ======== BANNER ======== -->
            <div class="top-banner728x90 text_center aligncenter bg_grey">
				<script type='text/javascript'>
                
                  var googletag = googletag || {};
                
                  googletag.cmd = googletag.cmd || [];
                
                  (function() {
                
                    var gads = document.createElement('script');
                
                    gads.async = true;
                
                    gads.type = 'text/javascript';
                
                    var useSSL = 'https:' == document.location.protocol;
                
                    gads.src = (useSSL ? 'https:' : 'http:') +
                
                      '//www.googletagservices.com/tag/js/gpt.js';
                
                    var node = document.getElementsByTagName('script')[0];
                
                    node.parentNode.insertBefore(gads, node);
                
                  })();
                
                </script>
                <script type='text/javascript'>
                
                  googletag.cmd.push(function() {
                
                    googletag.defineSlot('/47573522/travel_down_728x90', [728, 90], 'div-gpt-ad-1447638964347-0').addService(googletag.pubads());
                
                    googletag.pubads().enableSingleRequest();
                
                    googletag.enableServices();
                
                  });
                
                </script>            
                <a href="#" target="_blank">
                <!-- /47573522/travel_down_728x90 -->
                
                <div id='div-gpt-ad-1447638964347-0' style='height:90px; width:728px; margin:0 auto;'>
                
                <script type='text/javascript'>
                
                googletag.cmd.push(function() { googletag.display('div-gpt-ad-1447638964347-0'); });
                
                </script>
                
                </div>
                </a>
            </div>
            <!-- ======== HEADER ======== -->
            <?php include("header_menu.php");?>
            
            <!-- ======== Slider CONTAINER ======== -->
            <div class="fullwidth bg_grey">
                <div class="container">
                    <ul class="breadcrumb">
                      <li><a href="#">首頁</a></li>
                      <li><a href="#">單元名稱</a></li>
                      <li>頁面名稱</li>
                    </ul> 
                </div> 
            </div>
          
            
            
            <!-- ======== SECTION ======== -->
            <section id="page_wrapper">
                <div class="container">
                    <div class="row">
                        <!-- ======== MAIN CONTENT ======== -->
                        <div class="col col_8_of_12 main_content">
                                <div class="z_bluebk z_lineheigh30 z_fontcwhile z_padding10 z_margintop30">
                                        申請加入微笑聯盟
                                </div>
                                <div class="z_border_cc z_margintop10">
                                	<div class="col-sm-12"><div class="zone_left_blueline">店家名稱<span style="color:#F60">*</span></div></div>
                                    <div class="col-sm-12">                                                     
                                           <input name="abtn" type="text" class="form-control abtn" id="abtn">
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-sm-12"><div class="zone_left_blueline">類別<span style="color:#F60">*</span></div></div>
                                    <div class="form col-sm-6">                                                     
                                           <select name="bbtn" class="form-control bbtn" id="bbtn">
                                              <option>選擇位置&nbsp;&nbsp;</option>
                                              <option>上左</option>
                                              <option>上中</option>
                                              <option>上右</option>
                                              <option>下左</option>
                                              <option>下中</option>
                                              <option>下右</option>
                                            </select>  
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-sm-12"><div class="zone_left_blueline">縣市<span style="color:#F60">*</span></div></div>
                                    <div class="form col-sm-6">                                                     
                                           <select name="cbtn" class="form-control cbtn" id="cbtn">
                                              <option>選擇位置&nbsp;&nbsp;</option>
                                              <option>上左</option>
                                              <option>上中</option>
                                              <option>上右</option>
                                              <option>下左</option>
                                              <option>下中</option>
                                              <option>下右</option>
                                            </select>  
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-sm-12"><div class="zone_left_blueline">鄉鎮<span style="color:#F60">*</span></div></div>
                                    <div class="form col-sm-6">                                                     
                                           <select name="dbtn" class="form-control dbtn" id="dbtn">
                                              <option>選擇位置&nbsp;&nbsp;</option>
                                              <option>上左</option>
                                              <option>上中</option>
                                              <option>上右</option>
                                              <option>下左</option>
                                              <option>下中</option>
                                              <option>下右</option>
                                            </select>  
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-sm-12"><div class="zone_left_blueline">地址<span style="color:#F60">*</span></div></div>
                                    <div class="col-sm-12">                                                     
                                           <input name="ebtn" type="text" class="form-control ebtn" id="ebtn">
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-sm-12"><div class="zone_left_blueline">Google GPS 座標<span style="color:#F60">*</span></div></div>
                                    <div class="col-sm-6">                                                     
                                           <input name="hbtn" type="text" class="form-control hbtn" id="hbtn">
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-sm-12"><div class="zone_left_blueline">電話<span style="color:#F60">*</span></div></div>
                                    <div class="col-sm-6">                                                     
                                           <input name="fbtn" type="text" class="form-control fbtn" id="fbtn">
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-sm-12"><div class="zone_left_blueline">網址</div></div>
                                    <div class="col-sm-12">                                                     
                                           <input name="" type="text" class="form-control">
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-sm-12"><div class="zone_left_blueline">營業時間<span style="color:#F60">*</span></div></div>
                                    <div class="col-sm-6">                                                     
                                           <input name="gbtn" type="text" class="form-control gbtn" id="gbtn">
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-sm-12"><div class="zone_left_blueline">價格</div></div>
                                    <div class="col-sm-6">                                                     
                                           <input name="" type="text" class="form-control">
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-sm-12"><div class="zone_left_blueline">簡介</div></div>
                                    <div class="form col-sm-12">                                                     
                                           <textarea name="" cols="" rows="15" class="form-control"></textarea>
                                           <p style="text-align:left;" class="zone_a5font">輸入限制255個字</p>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-sm-12"><div class="zone_left_blueline">交通方式</div></div>
                                    <div class="form col-sm-12">                                                     
                                           <textarea name="" cols="" rows="15" class="form-control"></textarea>
                                           <p style="text-align:left;" class="zone_a5font">輸入限制255個字</p>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-sm-12"><div class="zone_left_blueline">Google GPS 座標<span style="color:#F60">*</span></div></div>
                                    <div class="col-sm-6">                                                     
                                           <input name="hbtn" type="text" class="form-control hbtn" id="hbtn">
                                    </div>
                                    <div class="clearfix"></div>
                                   <div class="col-sm-12"><div class="zone_left_blueline">上傳店家照片<span style="color:#F60">*</span></div></div>
                                    <div class="form col-sm-12">                                                     
                                           <input name="ibtn" type="file" class="form-control filestyle ibtn" id="ibtn" data-buttonText="選擇檔案"> 
                                           <p style="text-align:left;">說明：請上傳橫式圖片檔案（直式會強制變形），檔案大小限制在１Ｍ以下</p>                                     
                                    </div>
                                   <div class="clearfix"></div>
                                   <div class="col-sm-12"><div class="zone_left_blueline">上傳店家照片</div></div>
                                    <div class="form col-sm-12">                                                     
                                           <input name="" type="file" class="form-control filestyle"  data-buttonText="選擇檔案"> 
                                           <p style="text-align:left;">說明：請上傳橫式圖片檔案（直式會強制變形），檔案大小限制在１Ｍ以下</p>                                     
                                    </div>
                                    <div class="clearfix"></div>
                                   <div class="col-sm-12"><div class="zone_left_blueline">上傳店家照片</div></div>
                                    <div class="form col-sm-12">                                                     
                                           <input name="" type="file" class="form-control filestyle" data-buttonText="選擇檔案"> 
                                           <p style="text-align:left;">說明：請上傳橫式圖片檔案（直式會強制變形），檔案大小限制在１Ｍ以下</p>                                     
                                    </div>
                                    <div class="clearfix"></div> 
                                    <div class="col-sm-12"><div class="zone_left_blueline">到店打卡優惠</div></div>
                                    <div class="form col-sm-12">                                                     
                                           <textarea name="" cols="" rows="15" class="form-control"></textarea>
                                           <p style="text-align:left;" class="zone_a5font">歡迎提出店家專屬優惠活動，優惠活動將同步於手機app中呈現，鼓勵旅客到店拜訪</p>
                                    </div>
                                    <div class="clearfix"></div> 
                                    <div class="col-sm-12"><span style="color:#F60">*</span>訂閱「微笑台灣」電子報？
                   
                                          <label>
                                            <input type="radio" name="RadioGroup1" value="選項按鈕" id="RadioGroup1_0">
                                            是</label>
                                         
                                          <label>
                                            <input type="radio" name="RadioGroup1" value="選項按鈕" id="RadioGroup1_1">
                                            否</label>
                                           
                                    </div>                                  
                                    <div class="clearfix"></div>
                                    <div class="col-sm-12"><span style="color:#F60">*</span>願意收到天下雜誌群所發行之電子報或其它訊息？
                   
                                          <label>
                                            <input type="radio" name="RadioGroup1" value="選項按鈕" id="RadioGroup1_0">
                                            是</label>
                                         
                                          <label>
                                            <input type="radio" name="RadioGroup1" value="選項按鈕" id="RadioGroup1_1">
                                            否</label>
                                           
                                    </div>                                  
                                    <div class="clearfix"></div>
                                  <div style="height:20px;"></div>
                                	
                                </div>
                                <div class="z_border_cc z_margintop30">
                                	<div class="col-sm-12" style="text-align:center; line-height:25px;font-size:15px;">以下是非公開資料</div>
                                    <div class="clearfix"></div>
                                    <div style="border-bottom:1px solid #ccc; height:1px; margin-top:20px;"></div>
                                    <div class="clearfix"></div>
                                    <div class="col-sm-12"><div class="zone_left_blueline">申請帳號<span style="color:#F60">*</span></div></div>
                                    <div class="col-sm-6">                                                     
                                           <input name="jbtn" type="text" class="form-control jbtn" id="jbtn">
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-sm-12"><div class="zone_left_blueline">密碼<span style="color:#F60">*</span></div></div>
                                    <div class="col-sm-6">                                                     
                                           <input name="kbtn" type="text" class="form-control kbtn" id="kbtn">
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-sm-12"><div class="zone_left_blueline">密碼再確認<span style="color:#F60">*</span></div></div>
                                    <div class="col-sm-6">                                                     
                                           <input name="lbtn" type="text" class="form-control lbtn" id="lbtn">
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-sm-12"><div class="zone_left_blueline">聯絡人<span style="color:#F60">*</span></div></div>
                                    <div class="col-sm-6">                                                     
                                           <input name="mbtn" type="text" class="form-control mbtn" id="mbtn">
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-sm-12"><div class="zone_left_blueline">聯絡電話<span style="color:#F60">*</span></div></div>
                                    <div class="col-sm-6">                                                     
                                           <input name="nbtn" type="text" class="form-control nbtn" id="nbtn">
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-sm-12"><div class="zone_left_blueline">E-mail<span style="color:#F60">*</span></div></div>
                                    <div class="col-sm-6">                                                     
                                           <input name="obtn" type="text" class="form-control obtn" id="obtn">
                                    </div>
                                    <div class="clearfix"></div>
                                    <div style="height:20px;">&nbsp;</div>
                                    <div class="clearfix"></div>
                                	
                                </div>
                                <div class="modal-footer" style="text-align:center;">
                                  <button type="button" class="btn z_bluebk sendinputbtn" data-dismiss="modal">確認送出</button>
                                  <button type="button" class="btn z_bluebk">資料重填</button>
                                </div>
                       </div>
                        <!-- ======== SIDEBAR ======== -->
                        <div class="col col_4_of_12 sidebar sb_right300 xs_hide">
                        
                            <?php include("right578button.php");?>
                        </div>
                    </div>
                </div>
            </section>
            <!-- ======== FOOTER ======== -->
            <?php include("footer.php");?>

</div>
        </div>
        
        <!--leftmenu-->
    <div class="sb-slidebar sb-left sb-style-overlay">
        <!-- Main Navigation -->
        <?php include("left_menu.php");?>
        <!-- /Main Navigation -->
    </div>
        <!--righttmenu-->
		<div class="sb-slidebar sb-right sb-style-overlay">
          <div class="col col_12_of_12 sidebar">
    			<?php include("right578button.php");?>
          </div>
        </div>
        
        
        
        <!-- ======== JAVASCRIPTS ======== -->
        <script src="js/jquery.min.js"></script>
        <script src="js/jquery.ui.min.js"></script>
        <script src="js/jquery.fitvids.js"></script>
        <script src="js/jquery.owlcarousel.min.js"></script>
        <script src="js/jquery.magnific.popup.min.js"></script>
        <script src="js/jquery.parallax.min.js"></script>
        <script src="js/jquery.smooth.scroll.js"></script>
        <script src="js/jquery.init.js"></script>
         <script src="sys_plugin/oki_zippicker/oki_zipcode_mem.js"></script>
  <!-- MyWeather JS (needed) -->
  <script src="3dParty/MyWeather/js/MyWeather.js"></script>
  <script src="3dParty/bootstrap/js/bootstrap.minzone.js"></script>
  <script src="js/bootstrap-filestyle.min.js"></script>
  <script src="js/checkformtable.js"></script>
       
        
<!-- owl-carousel -->
    <script>
    $(document).ready(function() {
		$(":file").filestyle({input: false});
      $("#owl-demo").owlCarousel({
          navigation : false, // Show next and prev buttons
          slideSpeed : 300,
          paginationSpeed : 400,
          singleItem:true,
      autoPlay : true
     
          // "singleItem:true" is a shortcut for:
          // items : 1, 
          // itemsDesktop : false,
          // itemsDesktopSmall : false,
          // itemsTablet: false,
          // itemsMobile : false
      });
	  $('#myTab a').click(function(e) {
                e.preventDefault()
                $(this).tab('show')
            })
			
			$(".sendinputbtn").click(function(){
			_checkformnum=0;	
			_checkinput(".abtn",0,500,"close");
			_checkinput(".bbtn",0,500,"close");
			_checkinput(".cbtn",0,500,"close");
			_checkinput(".dbtn",0,500,"close");
			_checkinput(".ebtn",0,500,"close");
			_checkinput(".fbtn",0,500,"close");
			_checkinput(".gbtn",0,500,"close");
			_checkinput(".hbtn",0,500,"close");
			_checkinput(".ibtn",0,500,"close");
			_checkinput(".jbtn",0,500,"close");
			_checkinput(".kbtn",0,500,"close");
			_checkinput(".lbtn",0,500,"close");
			_checkinput(".mbtn",0,500,"close");
			_checkinput(".nbtn",0,500,"close");
			_checkinput(".obtn",0,500,"close");
			if(_checkformnum!=0){$("html,body").scrollTop(200);return false;}
			/*document.form1.submit();*/	
			return false;
	   })	
    });
  $('#t_owl_slider').owlCarousel({
          navigation : false, // Show next and prev buttons
          slideSpeed : 300,
          paginationSpeed : 400,
          singleItem:true,
      autoPlay : true,
})

  $('.shop_owl_slider').owlCarousel({
          navigation : true, // Show next and prev buttons
          pagination : false,
          slideSpeed : 300,
          paginationSpeed : 400,
          autoPlay : true,
loop:true,
    margin:10,
    items : 1, 
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        468:{
            items:1,
            nav:false
        },
        989:{
            items:1,
            nav:true,
            loop:false
        }
    }
})
  $('.postcard_owl_slider').owlCarousel({
          navigation : true, // Show next and prev buttons
          pagination : false,
          slideSpeed : 300,
          paginationSpeed : 400,
          autoPlay : true,
loop:true,
    margin:10,
    items : 3, 
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        468:{
            items:2,
            nav:false
        },
        989:{
            items:3,
            nav:true,
            loop:false
        }
    }
})
$('.postcard_owl_slider2').owlCarousel({
          navigation : true, // Show next and prev buttons
          pagination : false,
          slideSpeed : 300,
          paginationSpeed : 400,
          autoPlay : true,
loop:true,
    margin:10,
    items : 2, 
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        468:{
            items:2,
            nav:false
        },
        989:{
            items:3,
            nav:true,
            loop:false
        }
    }
})
    </script>
    
<!-- Slidebars -->
<script src="3dParty/slidebars/slidebars.min.js"></script>
<script>
    (function($) {
        $(document).ready(function() {
            $.slidebars();
      
      $("#schbtn").on("click",function(){ $("#schform").submit() });
      
        });
    }) (jQuery);
</script>

<!-- weather -->
<script type="text/javascript">

 $.MyWeather({
           elementid: "#localinfo",
           position: "right",
           showpopup: true,
           showinfo: true,
           temperature: "c",
           closeicon: false,
           showicon: true,
           showtemperature: true,
           showlocation: true,
           showip: false,
           size: 80,
           iconcolor: "#ffffff",
           fontcolor: "#ffffff",
},
function(City,Country, IP, Latitude, Longitude, Temperature){
// This callback function is called when we get those values. The callback is optional too.
}); 


</script> 

    </body>
</html>
<script>