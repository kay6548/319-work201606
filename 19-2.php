<!DOCTYPE html>
<html  lang="zh-Hant-TW">
    <head>
        <title>319鄉鎮</title>
        <!-- ======== META TAGS ======== -->
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <!-- ======== FAVICONS ======== -->
        <link rel="icon" href="favicon.ico">
        <link rel="apple-touch-icon" href="favicon.png">
        <!-- ======== STYLESHEETS ======== -->
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/typography.css">
        <link rel="stylesheet" href="css/fontawesome.css">
        <link rel="stylesheet" href="css/popup.css">
        <link rel="stylesheet" href="css/owlslider.css">
        <link rel="stylesheet" href="css/style.css">
        <link id="switch_style" rel="stylesheet" href="demo/main-color/blue.css">
        <!-- Slidebars CSS -->
        <link rel="stylesheet" href="3dParty/slidebars/slidebars.min.css">
        <!-- ======== RESPONSIVE ======== -->
        <link rel="stylesheet" media="(max-width:479px)" href="css/responsive-small.css">
        <link rel="stylesheet" media="(min-width:480px) and (max-width:768px)" href="css/responsive-0.css">
        <link rel="stylesheet" media="(min-width:769px) and (max-width:992px)" href="css/responsive-768.css">
        <link rel="stylesheet" media="(min-width:993px) and (max-width:1200px)" href="css/responsive-992.css">
        <link rel="stylesheet" media="(min-width:1201px)" href="css/responsive-1200.css">
        
        <!-- Slidebars CSS -->
        <link rel="stylesheet" href="3dParty/owl-carousel/owl.carousel.css">
        <link rel="stylesheet" href="3dParty/owl-carousel/owl.theme.css">

        <!-- MyWeather CSS (needed) -->
        <link rel="stylesheet" type="text/css" href="3dParty/MyWeather/css/MyWeather.css">
        <link rel="stylesheet" type="text/css" href="3dParty/bootstrap/css/zbootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="3dParty/bootstrap/css/bootstrap-theme.min.css">
        <link rel="stylesheet" type="text/css" href="css/zonestyle.css">



        <!-- ======== GOOGLE FONTS ======== -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Titillium+Web:400,300,300italic,400italic,700,700italic,600italic,600">
        <link href='https://fonts.googleapis.com/css?family=PT+Sans:400italic,400|Raleway:200italic,300,300italic|Oxygen:300' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Ubuntu:400,300,300italic' rel='stylesheet' type='text/css'>
        <!-- ======== GOOGLE ANALYTICS ======== -->
    <style>
    .owl-carousel{ display:block !important}

    #owl-demo .item img{
        display: block;
        width: 100%;
        height: auto;
    }


    </style>
    </head>
    <body>
        
        <!-- ======== WRAPPER ======== -->
        <div id="wrapper " class="wide">
        <div id="sb-site">
            <!-- ======== BANNER ======== -->
            <div class="top-banner728x90 text_center aligncenter bg_grey">
				<script type='text/javascript'>
                
                  var googletag = googletag || {};
                
                  googletag.cmd = googletag.cmd || [];
                
                  (function() {
                
                    var gads = document.createElement('script');
                
                    gads.async = true;
                
                    gads.type = 'text/javascript';
                
                    var useSSL = 'https:' == document.location.protocol;
                
                    gads.src = (useSSL ? 'https:' : 'http:') +
                
                      '//www.googletagservices.com/tag/js/gpt.js';
                
                    var node = document.getElementsByTagName('script')[0];
                
                    node.parentNode.insertBefore(gads, node);
                
                  })();
                
                </script>
                <script type='text/javascript'>
                
                  googletag.cmd.push(function() {
                
                    googletag.defineSlot('/47573522/travel_down_728x90', [728, 90], 'div-gpt-ad-1447638964347-0').addService(googletag.pubads());
                
                    googletag.pubads().enableSingleRequest();
                
                    googletag.enableServices();
                
                  });
                
                </script>            
                <a href="#" target="_blank">
                <!-- /47573522/travel_down_728x90 -->
                
                <div id='div-gpt-ad-1447638964347-0' style='height:90px; width:728px; margin:0 auto;'>
                
                <script type='text/javascript'>
                
                googletag.cmd.push(function() { googletag.display('div-gpt-ad-1447638964347-0'); });
                
                </script>
                
                </div>
                </a>
            </div>
            <!-- ======== HEADER ======== -->
            <?php include("header_menu.php");?>
            
            <!-- ======== Slider CONTAINER ======== -->
            <div class="fullwidth bg_grey">
                <div class="container">
                    <ul class="breadcrumb">
                      <li><a href="#">首頁</a></li>
                      <li><a href="#">單元名稱</a></li>
                      <li>頁面名稱</li>
                    </ul> 
                </div> 
            </div>
            
            
            <!-- ======== SECTION ======== -->
            <section id="page_wrapper">
                <div class="container">
                    <div class="row">
                        <!-- ======== MAIN CONTENT ======== -->
                        <div class="col col-sm-12 main_content">
                                <div class="">  
                                  <div class="title19 col-sm-12"><div class="font19">總共100筆</div></div>
                                  <div class="" style="color:#00a0da;font-size:20px; border-bottom:1px solid #ccc;">
                                  最新消息
                                  </div>
                                  <div class="a_box">
                                      <?php for($a=0;$a<3;$a++){?>
                                      <div class="z_margintop10" style="border-bottom:1px solid #ccc;"> 
                                        <div class="col-sm-2">
                                        <img src="img-main/person_pic_default.jpg"> 
                                        </div>
                                        <div class="col-sm-10">
                                                    <div class="col-sm-12" style="border-left:5px solid #00a0da;">
                                                     <div style="font-size:18px;"><?php echo $a+1;?>.融入社區的美術館--桃園市中壢區 </div>
                                                     <div class="z_contentfont">台北市 大安區</div>
                                                    </div>
                                                    <div class="z_contentfont" style="padding-left:20px;">
                                                    「歡迎各位朋友蒞臨襲園美術館參觀，這邊已幫大家準備了一杯茶，這茶葉是襲園自家茶園種植的有機茶。」一踏入襲園美術館，
    就像來到  一位相當有品味的老朋友家裡，Pauline是位笑容可掬、聲音甜美柔和的女孩，她是我們這次
                                                    </div>
                                        
                                        </div> 
                                        <div class="clearfix"></div>
                                        <div style="height:20px"></div> 
                                      </div>
                                      <?php }?>                 
                                  </div>
                                  <div class="modal-footer col-sm-12" style="text-align:center;">
                                  <button type="button" class="btn z_bluebk a_box_btn">點撃查看更多相關搜尋</button>
                                  </div>
                               </div> 
                               <div class="z_margintop30">  
                                  <div class="title19 col-sm-12"><div class="font19">總共100筆</div></div>
                                  <div class="" style="color:#00a0da;font-size:20px; border-bottom:1px solid #ccc;">
                                  鄉鎮素描
                                  </div>
                                  <div class="b_box">
                                      <?php for($a=0;$a<3;$a++){?>
                                      <div class="z_margintop10" style="border-bottom:1px solid #ccc;"> 
                                        <div class="col-sm-2">
                                        <img src="img-main/person_pic_default.jpg"> 
                                        </div>
                                        <div class="col-sm-10">
                                                    <div class="col-sm-12" style="border-left:5px solid #00a0da;">
                                                     <div style="font-size:18px;"><?php echo $a+1;?>.融入社區的美術館--桃園市中壢區 </div>
                                                     <div class="z_contentfont">台北市 大安區</div>
                                                    </div>
                                                    <div class="z_contentfont" style="padding-left:20px;">
                                                    「歡迎各位朋友蒞臨襲園美術館參觀，這邊已幫大家準備了一杯茶，這茶葉是襲園自家茶園種植的有機茶。」一踏入襲園美術館，
    就像來到  一位相當有品味的老朋友家裡，Pauline是位笑容可掬、聲音甜美柔和的女孩，她是我們這次
                                                    </div>
                                        
                                        </div> 
                                        <div class="clearfix"></div>
                                        <div style="height:20px"></div> 
                                      </div>
                                      <?php }?>                 
                                  </div>
                                  <div class="modal-footer col-sm-12" style="text-align:center;">
                                  <button type="button" class="btn z_bluebk b_box_btn">點撃查看更多相關搜尋</button>
                                  </div>
                               </div>  
                               <div class="z_margintop30">  
                                  <div class="title19 col-sm-12"><div class="font19">總共100筆</div></div>
                                  <div class="" style="color:#00a0da;font-size:20px; border-bottom:1px solid #ccc;">
                                  微笑聯盟
                                  </div>
                                  <div class="c_box">
                                      <?php for($a=0;$a<3;$a++){?>
                                      <div class="z_margintop10" style="border-bottom:1px solid #ccc;"> 
                                        <div class="col-sm-2">
                                        <img src="img-main/person_pic_default.jpg"> 
                                        </div>
                                        <div class="col-sm-10">
                                                    <div class="col-sm-12" style="border-left:5px solid #00a0da;">
                                                     <div style="font-size:18px;"><?php echo $a+1;?>.融入社區的美術館--桃園市中壢區 </div>
                                                     <div class="z_contentfont">台北市 大安區</div>
                                                    </div>
                                                    <div class="z_contentfont" style="padding-left:20px;">
                                                    「歡迎各位朋友蒞臨襲園美術館參觀，這邊已幫大家準備了一杯茶，這茶葉是襲園自家茶園種植的有機茶。」一踏入襲園美術館，
    就像來到  一位相當有品味的老朋友家裡，Pauline是位笑容可掬、聲音甜美柔和的女孩，她是我們這次
                                                    </div>
                                        
                                        </div> 
                                        <div class="clearfix"></div>
                                        <div style="height:20px"></div> 
                                      </div>
                                      <?php }?>                 
                                  </div>
                                  <div class="modal-footer col-sm-12" style="text-align:center;">
                                  <button type="button" class="btn z_bluebk c_box_btn">點撃查看更多相關搜尋</button>
                                  </div>
                               </div>    
                               <div class="z_margintop30">  
                                  <div class="title19 col-sm-12"><div class="font19">總共100筆</div></div>
                                  <div class="" style="color:#00a0da;font-size:20px; border-bottom:1px solid #ccc;">
                                  旅行明信片
                                  </div>
                                  <div class="d_box">
                                      <?php for($a=0;$a<3;$a++){?>
                                      <div class="z_margintop10" style="border-bottom:1px solid #ccc;"> 
                                        <div class="col-sm-2">
                                        <img src="img-main/person_pic_default.jpg"> 
                                        </div>
                                        <div class="col-sm-10">
                                                    <div class="col-sm-12" style="border-left:5px solid #00a0da;">
                                                     <div style="font-size:18px;"><?php echo $a+1;?>.融入社區的美術館--桃園市中壢區 </div>
                                                     <div class="z_contentfont">台北市 大安區</div>
                                                    </div>
                                                    <div class="z_contentfont" style="padding-left:20px;">
                                                    「歡迎各位朋友蒞臨襲園美術館參觀，這邊已幫大家準備了一杯茶，這茶葉是襲園自家茶園種植的有機茶。」一踏入襲園美術館，
    就像來到  一位相當有品味的老朋友家裡，Pauline是位笑容可掬、聲音甜美柔和的女孩，她是我們這次
                                                    </div>
                                        
                                        </div> 
                                        <div class="clearfix"></div>
                                        <div style="height:20px"></div> 
                                      </div>
                                      <?php }?>                 
                                  </div>
                                  <div class="modal-footer col-sm-12" style="text-align:center;">
                                  <button type="button" class="btn z_bluebk d_box_btn">點撃查看更多相關搜尋</button>
                                  </div>
                               </div> 
                               <div class="z_margintop30">  
                                  <div class="title19 col-sm-12"><div class="font19">總共100筆</div></div>
                                  <div class="" style="color:#00a0da;font-size:20px; border-bottom:1px solid #ccc;">
                                  旅人專欄
                                  </div>
                                  <div class="e_box">
                                      <?php for($a=0;$a<3;$a++){?>
                                      <div class="z_margintop10" style="border-bottom:1px solid #ccc;"> 
                                        <div class="col-sm-2">
                                        <img src="img-main/person_pic_default.jpg"> 
                                        </div>
                                        <div class="col-sm-10">
                                                    <div class="col-sm-12" style="border-left:5px solid #00a0da;">
                                                     <div style="font-size:18px;"><?php echo $a+1;?>.融入社區的美術館--桃園市中壢區 </div>
                                                     <div class="z_contentfont">台北市 大安區</div>
                                                    </div>
                                                    <div class="z_contentfont" style="padding-left:20px;">
                                                    「歡迎各位朋友蒞臨襲園美術館參觀，這邊已幫大家準備了一杯茶，這茶葉是襲園自家茶園種植的有機茶。」一踏入襲園美術館，
    就像來到  一位相當有品味的老朋友家裡，Pauline是位笑容可掬、聲音甜美柔和的女孩，她是我們這次
                                                    </div>
                                        
                                        </div> 
                                        <div class="clearfix"></div>
                                        <div style="height:20px"></div> 
                                      </div>
                                      <?php }?>                 
                                  </div>
                                  <div class="modal-footer col-sm-12" style="text-align:center;">
                                  <button type="button" class="btn z_bluebk e_box_btn">點撃查看更多相關搜尋</button>
                                  </div>
                               </div>       
                                  
                      </div>
                        <!-- ======== 12of12 ======== -->
                    </div>
                </div>
                <!-- ======== container ======== -->
            </section>
            <!-- ======== FOOTER ======== -->
            <?php include("footer.php");?>

</div>
        </div>
        
        <!--leftmenu-->
    <div class="sb-slidebar sb-left sb-style-overlay">
        <!-- Main Navigation -->
        <?php include("left_menu.php");?>
        <!-- /Main Navigation -->
    </div>
        <!--righttmenu-->
		<div class="sb-slidebar sb-right sb-style-overlay">
          <div class="col col_12_of_12 sidebar">
          		<?php include("right578button.php");?>   
          </div>
        </div>
        
        
        
        <!-- ======== JAVASCRIPTS ======== -->
        <script src="js/jquery.min.js"></script>
        <script src="js/jquery.ui.min.js"></script>
        <script src="js/jquery.fitvids.js"></script>
        <script src="js/jquery.owlcarousel.min.js"></script>
        <script src="js/jquery.magnific.popup.min.js"></script>
        <script src="js/jquery.parallax.min.js"></script>
        <script src="js/jquery.smooth.scroll.js"></script>
        <script src="js/jquery.init.js"></script>
         <script src="sys_plugin/oki_zippicker/oki_zipcode_mem.js"></script>
  <!-- MyWeather JS (needed) -->
    <script src="3dParty/MyWeather/js/MyWeather.js"></script>
    <script src="3dParty/bootstrap/js/bootstrap.minzone.js"></script>
    <script src="js/initfiscroll.js"></script>
       
        
<!-- owl-carousel -->
    <script>
    $(document).ready(function() {
      $("#owl-demo").owlCarousel({
          navigation : false, // Show next and prev buttons
          slideSpeed : 300,
          paginationSpeed : 400,
          singleItem:true,
      autoPlay : true
     
          // "singleItem:true" is a shortcut for:
          // items : 1, 
          // itemsDesktop : false,
          // itemsDesktopSmall : false,
          // itemsTablet: false,
          // itemsMobile : false
      });
	  $('#myTab a').click(function(e) {
                e.preventDefault()
                $(this).tab('show')
            })
    });
  $('#t_owl_slider').owlCarousel({
          navigation : false, // Show next and prev buttons
          slideSpeed : 300,
          paginationSpeed : 400,
          singleItem:true,
      autoPlay : true,
})

  $('.shop_owl_slider').owlCarousel({
          navigation : true, // Show next and prev buttons
          pagination : false,
          slideSpeed : 300,
          paginationSpeed : 400,
          autoPlay : true,
loop:true,
    margin:10,
    items : 3, 
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        468:{
            items:2,
            nav:false
        },
        989:{
            items:3,
            nav:true,
            loop:false
        }
    }
})
  $('.postcard_owl_slider').owlCarousel({
          navigation : true, // Show next and prev buttons
          pagination : false,
          slideSpeed : 300,
          paginationSpeed : 400,
          autoPlay : true,
loop:true,
    margin:10,
    items : 3, 
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        468:{
            items:2,
            nav:false
        },
        989:{
            items:3,
            nav:true,
            loop:false
        }
    }
})
$('.postcard_owl_slider2').owlCarousel({
          navigation : true, // Show next and prev buttons
          pagination : false,
          slideSpeed : 300,
          paginationSpeed : 400,
          autoPlay : true,
loop:true,
    margin:10,
    items : 2, 
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        468:{
            items:2,
            nav:false
        },
        989:{
            items:3,
            nav:true,
            loop:false
        }
    }
})
    </script>
    
<!-- Slidebars -->
<script src="3dParty/slidebars/slidebars.min.js"></script>
<script>
    (function($) {
        $(document).ready(function() {
            $.slidebars();
      
      $("#schbtn").on("click",function(){ $("#schform").submit() });
      
        });
    }) (jQuery);
</script>

<!-- weather -->
<script type="text/javascript">

 $.MyWeather({
           elementid: "#localinfo",
           position: "right",
           showpopup: true,
           showinfo: true,
           temperature: "c",
           closeicon: false,
           showicon: true,
           showtemperature: true,
           showlocation: true,
           showip: false,
           size: 80,
           iconcolor: "#ffffff",
           fontcolor: "#ffffff",
},
function(City,Country, IP, Latitude, Longitude, Temperature){
// This callback function is called when we get those values. The callback is optional too.
}); 


</script> 

    </body>
</html>
<script>