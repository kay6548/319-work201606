            <header id="header" class="style_1">
                <!-- ======== BODY ======== -->
                <div class="header_body">
                    <div class="container">
                        <!-- ======== LOGO TEXT ========
                        <div class="logo_brand">
                            <h1><a href="index.html">GoodStart</a></h1>
                            <h2>Premium Magazine Theme</h2>
                        </div> -->
                        <!-- ======== LOGO IMG ======== -->
                        <div class="logo_brand">
                            <a href="index.php">
                                <img src="img-main/logo_smiletaiwan.png" alt="319鄉鎮">
                                <span class="clearfix"></span>
                            </a>
                        </div>
                        <!-- ======== MENU ======== -->
                        <div class="sticky_menu header_menu light">
                            <div class="container">
                                <!-- ======== BUTTON FOR MOBILE MENU ======== -->
                                <a class="open_menu_mobile pull-left">
                                    <div class="sb-open-left sb-close"><i class="fa fa-bars"></i>
                                    </div>
                                </a>
                                <a class="open_menu_mobile pull-right">
                                    <div class="sb-open-right sb-close"><i class="fa fa-ellipsis-h"></i>
                                    </div>
                                </a>
                                <!-- ======== MENU ======== -->
                                <nav class="site_navigation">
                                    <ul class="menu pull-right">
                                        <li class="search">
                                            <div class="search_block">
                                                <form name="schform" id="schform" enctype="multipart/form-data" action="?c=search" method="get">
                                                    <input type="hidden" name="c" value="search">
                                                    <input type="search" name="schtxt" id="schtxt" placeholder="">
                                                    <i class="fa fa-search text_white" id="schbtn"></i>
                                                </form>
                                            </div>
                                        </li>
                                        <li class=""><a class="active" href="#">特別企劃：12個夏天</a>
                                        </li>
                                        <li class="menu-item-has-children"><a href="11-2.php">微笑聯盟</a>
                                            <ul class="sub-menu">
                                                <li><a href="12-2.php">北部</a></li>
                                                <li><a href="12-2.php">中部</a></li>
                                                <li><a href="12-2.php">南部</a></li>
                                                <li><a href="12-2.php">東部</a></li>
                                                <li><a href="12-2.php">西部</a></li>
                                            </ul>
                                        </li>
                                        <li class="menu-item-has-children"><a href="#">旅人專欄</a>
                                            <ul class="sub-menu">
                                                <li><a href="#">駐站旅人</a></li>
                                                <li><a href="#">駐站旅人</a></li>
                                                <li><a href="#">駐站旅人</a></li>
                                                <li><a href="#">駐站旅人</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="6-2.php">旅行明信片</a></li>
                                        <li class="menu-item-has-children"><a href="22-2.php">會員專區</a>
                                            <ul class="sub-menu">
                                                <li><a href="#">會員登入</a></li>
                                                <li><a href="#">加入會員</a></li>
                                            </ul>
                                        </li>
                                        <li>
                                            <div class="toggleSideMenu sb-toggle-left navbar-left">
                                                <i class="fa fa-bars fa-2x"></i>
                                            </div>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
