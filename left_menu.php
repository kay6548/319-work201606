        <div class="sideMenu">
            <ul class="sideMenuMain">
                <li><a href="index.php">首頁</a></li>
                <li>
                    <a class="" href="9-2.php">最新消息</a>
                </li>
                <li>
                    <a class="" href="3-2.php">鄉鎮素描</a>
                </li>
                <li class="moreItem">
                    <a>微笑聯盟</a>
                    <ul class="sudeMenuSub">
                        <li><a href="11-2.php">微笑聯盟</a></li>
                        <li><a href="12-2.php">查詢微笑聯盟</a></li>
                        <li><a href="13-2.php">微笑聯盟簡介</a></li>
                        <li><a href="17-2.php">加入微笑聯盟</a></li>
                        <li><a href="16-2.php">雲端護照任務</a></li>
                        <li><a href="20-2.php">雲端護照APP</a></li>
                    </ul>
                </li>
                <li class="moreItem">
                    <a>旅人專欄</a>
                    <ul class="sudeMenuSub">
                        <li><a href="http://319travel.kay.tw/" target="_blank">專欄文章</a></li>
                        <li><a href="#">數位特企</a></li>
                    </ul>
                </li>
                <li class="moreItem">
                    <a>旅行明信片</a>
                    <ul class="sudeMenuSub">
                        <li><a href="6-2.php">精選明信片</a></li>
                        <li><a href="7-2.php">最新上傳</a></li>
                    </ul>
                </li>
                <li class="moreItem">
                    <a>會員專區</a>
                    <ul class="sudeMenuSub">
                        <li><a href="22-2.php">  個人首頁</a></li>
                        <li><a href="12.php">  編輯會員資料</a></li>
                        <li><a href="4.php">新增旅行明信片</a></li>
                        <li><a href="11.php">  投稿專欄文章</a></li>
                        <li><a href="15.php">  微笑聯盟管理</a></li>
                        <li><a href="16.php">  我的幸福點數</a></li>
                        <li><a href="17.php">  集點兌換贈品</a></li>
                        <li><a href="24.php">  使用空間限制</a></li>
                        <li><a href="25.php">  客服中心</a></li>
                    </ul>
                </li>
            </ul>
        </div>
