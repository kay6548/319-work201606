<!DOCTYPE html>
<html lang="zh-Hant-TW">

<head>
    <title>319鄉鎮</title>
    <!-- ======== META TAGS ======== -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- ======== FAVICONS ======== -->
    <link rel="icon" href="favicon.ico">
    <link rel="apple-touch-icon" href="favicon.png">
    <!-- ======== STYLESHEETS ======== -->
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/typography.css">
    <link rel="stylesheet" href="css/fontawesome.css">
    <link rel="stylesheet" href="css/popup.css">
    <link rel="stylesheet" href="css/owlslider.css">
    <link rel="stylesheet" href="css/style.css">
    <link id="switch_style" rel="stylesheet" href="demo/main-color/blue.css">
    <!-- Slidebars CSS -->
    <link rel="stylesheet" href="3dParty/slidebars/slidebars.min.css">
    <!-- ======== RESPONSIVE ======== -->
    <link rel="stylesheet" media="(max-width:479px)" href="css/responsive-small.css">
    <link rel="stylesheet" media="(min-width:480px) and (max-width:768px)" href="css/responsive-0.css">
    <link rel="stylesheet" media="(min-width:769px) and (max-width:992px)" href="css/responsive-768.css">
    <link rel="stylesheet" media="(min-width:993px) and (max-width:1200px)" href="css/responsive-992.css">
    <link rel="stylesheet" media="(min-width:1201px)" href="css/responsive-1200.css">
    <!-- Slidebars CSS -->
    <link rel="stylesheet" href="3dParty/owl-carousel/owl.carousel.css">
    <link rel="stylesheet" href="3dParty/owl-carousel/owl.theme.css">
    <!-- MyWeather CSS (needed) -->
    <link rel="stylesheet" type="text/css" href="3dParty/MyWeather/css/MyWeather.css">
    <!-- ======== GOOGLE FONTS ======== -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Titillium+Web:400,300,300italic,400italic,700,700italic,600italic,600">
    <link href='https://fonts.googleapis.com/css?family=PT+Sans:400italic,400|Raleway:200italic,300,300italic|Oxygen:300' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Ubuntu:400,300,300italic' rel='stylesheet' type='text/css'>
    <!-- ======== GOOGLE ANALYTICS ======== -->
    <style>
    .owl-carousel {
        display: block !important
    }
    
    #owl-demo .item img {
        display: block;
        width: 100%;
        height: auto;
    }
    </style>
</head>

<body>
    <!-- ======== WRAPPER ======== -->
    <div id="wrapper " class="wide">
        <div id="sb-site">
            <!-- ======== BANNER ======== -->
            <div class="top-banner728x90 text_center aligncenter bg_grey">
                <script type='text/javascript'>
                var googletag = googletag || {};

                googletag.cmd = googletag.cmd || [];

                (function() {

                    var gads = document.createElement('script');

                    gads.async = true;

                    gads.type = 'text/javascript';

                    var useSSL = 'https:' == document.location.protocol;

                    gads.src = (useSSL ? 'https:' : 'http:') +

                        '//www.googletagservices.com/tag/js/gpt.js';

                    var node = document.getElementsByTagName('script')[0];

                    node.parentNode.insertBefore(gads, node);

                })();
                </script>
                <script type='text/javascript'>
                googletag.cmd.push(function() {

                    googletag.defineSlot('/47573522/travel_down_728x90', [728, 90], 'div-gpt-ad-1447638964347-0').addService(googletag.pubads());

                    googletag.pubads().enableSingleRequest();

                    googletag.enableServices();

                });
                </script>
                <a href="#" target="_blank">
                    <!-- /47573522/travel_down_728x90 -->
                    <div id='div-gpt-ad-1447638964347-0' style='height:90px; width:728px; margin:0 auto;'>
                        <script type='text/javascript'>
                        googletag.cmd.push(function() {
                            googletag.display('div-gpt-ad-1447638964347-0');
                        });
                        </script>
                    </div>
                </a>
            </div>
            <!-- ======== HEADER ======== -->
            <?php include("header_menu.php");?>

            <!-- ======== Slider CONTAINER ======== -->
            <section class="slider_kv">
                <div class="container">
                    <div class="row">
                        <div id="localinfo"></div>
                    </div>
                </div>
                <!-- OWL Slider -->
                <div id="t_owl_slider" class="owl-carousel owl-theme">
                    <div class="item" style="background-image:url(img-main/kvbanner/kv-01.jpg)">
                        <img src="img-main/kvbanner/kv-00.gif" alt="">
                    </div>
                    <div class="item" style="background-image:url(img-main/kvbanner/kv-01.jpg)">
                        <img src="img-main/kvbanner/kv-00.gif" alt="">
                    </div>
                    <div class="item" style="background-image:url(img-main/kvbanner/kv-01.jpg)">
                        <img src="img-main/kvbanner/kv-00.gif" alt="">
                    </div>
                </div>
                <!-- End OWL Slider -->
            </section>

            <!-- ======== SECTION ======== -->
            <section id="page_wrapper">
                <div class="container">
                    <div class="row">
                        <!-- ======== MAIN CONTENT ======== -->
                        <div class="col col_8_of_12 main_content">
                            <!-- ======== SEARCH ======== -->
                            <div class="widget widget_search ">
                                <div class="widget_title text_center">
                                    <img src="img-main/title_select.jpg" alt="" />
                                    <div class="tips">目前文章顯示區域：<span>全區</span></div>
                                </div>
                                <div class="row">
                                    <form class="search_form" name="schzone" id="schzone" action="?c=search" method="get" enctype="multipart/form-data">
                                        <div class="col col_5_of_12">
                                            <select name=city id="#city" class="" area="#area">
                                                <option>請選擇縣市</option>
                                                <option></option>
                                                <option></option>
                                                <option></option>
                                            </select>
                                        </div>
                                        <div class="col col_5_of_12">
                                            <select id="area" name="area" class="">
                                                <option>請選擇鄉鎮</option>
                                                <option></option>
                                                <option></option>
                                                <option></option>
                                            </select>
                                        </div>
                                        <div class="col col_2_of_12">
                                            <a class="btn btn_large margin-top-10" href="#">確認送出</a>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <!-- ======== 精選文章 ======== -->
                            <div class="article_list standard no_paddong">
                                <div id="owl-demo" class="owl-carousel owl-theme ">
                                    <div class="item">
                                        <div class="item_header">
                                            <a href="#">
                                                <img src="demo/banners/demo750x450-1.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="item_wrapper">
                                            <div class="item_pattem">
                                                <div class="circle-org">88,888
                                                    <br>Follow</div>
                                                <div class="title">Follow Me</div>
                                            </div>
                                            <div class="item_content">
                                                <a href="#">
                                                    <h4>文章標題26字文章標題26字文章標題26字文章標題26字</h4>
                                                    <div class="content_meta">
                                                    </div>
                                                    <p>夏末 趁著陽光依舊耀眼 來到了座落在台南市北門區井仔腳的瓦盤鹽田 位於台南北門的井仔腳瓦盤鹽田是此區的第一座鹽田，也是現存最古老的瓦盤鹽田
                                                    </p>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="item_header">
                                            <a href="#">
                                                <img src="img-main/demo750x450.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="item_wrapper">
                                            <div class="item_pattem">
                                                <div class="circle-org">88,888
                                                    <br>Follow</div>
                                                <div class="title">Follow Me</div>
                                            </div>
                                            <div class="item_content">
                                                <a href="#">
                                                    <h4>文章標題26字文章標題26字文章標題26字文章標題26字</h4>
                                                    <div class="content_meta">
                                                    </div>
                                                    <p>夏末 趁著陽光依舊耀眼 來到了座落在台南市北門區井仔腳的瓦盤鹽田 位於台南北門的井仔腳瓦盤鹽田是此區的第一座鹽田，也是現存最古老的瓦盤鹽田
                                                    </p>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="item_header">
                                            <a href="#">
                                                <img src="img-main/demo750x450.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="item_wrapper">
                                            <div class="item_pattem">
                                                <div class="circle-org">88,888
                                                    <br>Follow</div>
                                                <div class="title">Follow Me</div>
                                            </div>
                                            <div class="item_content">
                                                <a href="#">
                                                    <h4>文章標題26字文章標題26字文章標題26字文章標題26字</h4>
                                                    <div class="content_meta">
                                                    </div>
                                                    <p>夏末 趁著陽光依舊耀眼 來到了座落在台南市北門區井仔腳的瓦盤鹽田 位於台南北門的井仔腳瓦盤鹽田是此區的第一座鹽田，也是現存最古老的瓦盤鹽田
                                                    </p>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="item_header">
                                            <a href="#">
                                                <img src="img-main/demo750x450.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="item_wrapper">
                                            <div class="item_pattem">
                                                <div class="circle-org">88,888
                                                    <br>Follow</div>
                                                <div class="title">Follow Me</div>
                                            </div>
                                            <div class="item_content">
                                                <a href="#">
                                                    <h4>文章標題26字文章標題26字文章標題26字文章標題26字</h4>
                                                    <div class="content_meta">
                                                    </div>
                                                    <p>夏末 趁著陽光依舊耀眼 來到了座落在台南市北門區井仔腳的瓦盤鹽田 位於台南北門的井仔腳瓦盤鹽田是此區的第一座鹽田，也是現存最古老的瓦盤鹽田
                                                    </p>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="item_header">
                                            <a href="#">
                                                <img src="img-main/demo750x450.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="item_wrapper">
                                            <div class="item_pattem">
                                                <div class="circle-org">88,888
                                                    <br>Follow</div>
                                                <div class="title">Follow Me</div>
                                            </div>
                                            <div class="item_content">
                                                <a href="#">
                                                    <h4>文章標題26字文章標題26字文章標題26字文章標題26字</h4>
                                                    <div class="content_meta">
                                                    </div>
                                                    <p>夏末 趁著陽光依舊耀眼 來到了座落在台南市北門區井仔腳的瓦盤鹽田 位於台南北門的井仔腳瓦盤鹽田是此區的第一座鹽田，也是現存最古老的瓦盤鹽田
                                                    </p>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- ======== 旅人文章 ======== -->
                            <div class="widget widget_search ">
                                <div class="widget_title text_center">
                                    <img src="img-main/title_post.jpg" alt="" />
                                </div>
                                <div class="row">
                                    <div class="col col_6_of_12">
                                        <div class="row item writerphoto-s">
                                            <div class="item_header padding_10">
                                                <img src="demo/avatars/1.jpg" alt="">
                                            </div>
                                            <div class="item_wrapper">
                                                <div class="item_info padding_10">
                                                    <h5 class="author text_left no_paddong_bottom text_grey">旅人 Sandy</h5>
                                                    <div class="comment">清潔淨化海灘活動，靠你我的小小力量累積而成</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col col_6_of_12">
                                        <div class="row item writerphoto-s">
                                            <div class="item_header padding_10">
                                                <img src="demo/avatars/1.jpg" alt="">
                                            </div>
                                            <div class="item_wrapper">
                                                <div class="item_info padding_10">
                                                    <h5 class="author text_left no_paddong_bottom text_grey">旅人 Sandy</h5>
                                                    <div class="comment">清潔淨化海灘活動，靠你我的小小力量累積而成</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col col_6_of_12">
                                        <div class="row item writerphoto-s">
                                            <div class="item_header padding_10">
                                                <img src="demo/avatars/1.jpg" alt="">
                                            </div>
                                            <div class="item_wrapper">
                                                <div class="item_info padding_10">
                                                    <h5 class="author text_left no_paddong_bottom text_grey">旅人 Sandy</h5>
                                                    <div class="comment">清潔淨化海灘活動，靠你我的小小力量累積而成</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col col_6_of_12">
                                        <div class="row item writerphoto-s">
                                            <div class="item_header padding_10">
                                                <img src="demo/avatars/1.jpg" alt="">
                                            </div>
                                            <div class="item_wrapper">
                                                <div class="item_info padding_10">
                                                    <h5 class="author text_left no_paddong_bottom text_grey">旅人 Sandy</h5>
                                                    <div class="comment">清潔淨化海灘活動，靠你我的小小力量累積而成</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- ======== CONTENT BANNER======== -->
                            <div class="content_banner">
                                <div class="banner">
                                    <a href="#" target="_blank">
                                        <img src="demo/banners/728x90.jpg" alt="">
                                    </a>
                                </div>
                            </div>
                            <!-- ======== 本地任務與最新消息 ======== -->
                            <div class="block_with_tabs margin-top-10 padding_5">
                                <div id="tab-x2">
                                    <div class="row">
                                        <div class="col col_6_of_12">
                                            <!-- ======== ARTICLE LIST BIG ======== -->
                                            <div class="article_list_big">
                                                <div class="thumb_wrapper">
                                                    <a href="#" class="" title="">
                                                        <img src="demo/banners/demo750x450-2.jpg" alt="">
                                                    </a>
                                                    <div class="title">
                                                        Smile Mission
                                                        <h5>本地任務</h5>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col col_6_of_12">
                                            <!-- ======== ARTICLE LIST BIG ======== -->
                                            <div class="article_list_big">
                                                <div class="thumb_wrapper">
                                                    <a href="#" class="" title="">
                                                        <img src="demo/banners/demo750x450-3.jpg" alt="">
                                                    </a>
                                                    <div class="title">
                                                        Smile News
                                                        <h5>最新消息</h5>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- ======== 在地聯盟店家 ======== -->
                            <div class="widget widget_search ">
                                <div class="widget_title text_center">
                                    <img src="img-main/title_shop.jpg" alt="" />
                                </div>
                            </div>
                            <!-- OWL Slider -->
                            <div id="" class="shop_owl_slider item_slider owl-carousel owl-theme">
                                <div class="item padding_5" style="">
                                    <div class="medium_article_list">
                                        <div class="thumb_wrapper">
                                            <div class="type_box">
                                                <div class="type">食</div>
                                            </div>
                                            <a href="#" class="" title="">
                                                <img src="demo/banners/demo750x450-4.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="content_wrapper padding_5">
                                            <h5><a href="#">在地聯盟店家名稱</a></h5>
                                            <p class="text_grey">高雄市 博愛特區</p>
                                            <div class="rate_type margin-top-10">
                                                <div class="star-rating" title="Rated 4.50 out of 5">
                                                    <span style="width: 90%">4.50 out of 5</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item padding_5" style="">
                                    <div class="medium_article_list">
                                        <div class="thumb_wrapper">
                                            <div class="type_box">
                                                <div class="type">食</div>
                                            </div>
                                            <a href="#" class="" title="">
                                                <img src="demo/banners/demo750x450-5.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="content_wrapper padding_5">
                                            <h5><a href="#">在地聯盟店家名稱</a></h5>
                                            <p class="text_grey">高雄市 博愛特區</p>
                                            <div class="rate_type margin-top-10">
                                                <div class="star-rating" title="Rated 4.50 out of 5">
                                                    <span style="width: 90%">4.50 out of 5</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item padding_5" style="">
                                    <div class="medium_article_list">
                                        <div class="thumb_wrapper">
                                            <div class="type_box">
                                                <div class="type">食</div>
                                            </div>
                                            <a href="#" class="" title="">
                                                <img src="demo/banners/demo750x450-6.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="content_wrapper padding_5">
                                            <h5><a href="#">在地聯盟店家名稱</a></h5>
                                            <p class="text_grey">高雄市 博愛特區</p>
                                            <div class="rate_type margin-top-10">
                                                <div class="star-rating" title="Rated 4.50 out of 5">
                                                    <span style="width: 90%">4.50 out of 5</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item padding_5" style="">
                                    <div class="medium_article_list">
                                        <div class="thumb_wrapper">
                                            <div class="type_box">
                                                <div class="type">食</div>
                                            </div>
                                            <a href="#" class="" title="">
                                                <img src="demo/banners/demo750x450-4.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="content_wrapper padding_5">
                                            <h5><a href="#">在地聯盟店家名稱</a></h5>
                                            <p class="text_grey">高雄市 博愛特區</p>
                                            <div class="rate_type margin-top-10">
                                                <div class="star-rating" title="Rated 4.50 out of 5">
                                                    <span style="width: 90%">4.50 out of 5</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item padding_5" style="">
                                    <div class="medium_article_list">
                                        <div class="thumb_wrapper">
                                            <div class="type_box">
                                                <div class="type">食</div>
                                            </div>
                                            <a href="#" class="" title="">
                                                <img src="demo/banners/demo750x450-5.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="content_wrapper padding_5">
                                            <h5><a href="#">在地聯盟店家名稱</a></h5>
                                            <p class="text_grey">高雄市 博愛特區</p>
                                            <div class="rate_type margin-top-10">
                                                <div class="star-rating" title="Rated 4.50 out of 5">
                                                    <span style="width: 90%">4.50 out of 5</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item padding_5">
                                    <div class="medium_article_list">
                                        <div class="thumb_wrapper">
                                            <div class="type_box">
                                                <div class="type">食</div>
                                            </div>
                                            <a href="#" class="" title="">
                                                <img src="demo/banners/demo750x450-6.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="content_wrapper padding_5">
                                            <h5><a href="#">在地聯盟店家名稱</a></h5>
                                            <p class="text_grey">高雄市 博愛特區</p>
                                            <div class="rate_type margin-top-10">
                                                <div class="star-rating" title="Rated 4.50 out of 5">
                                                    <span style="width: 90%">4.50 out of 5</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item padding_5">
                                    <div class="medium_article_list">
                                        <div class="thumb_wrapper">
                                            <div class="type_box">
                                                <div class="type">食</div>
                                            </div>
                                            <a href="#" class="" title="">
                                                <img src="demo/banners/demo750x450-4.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="content_wrapper padding_5">
                                            <h5><a href="#">在地聯盟店家名稱</a></h5>
                                            <p class="text_grey">高雄市 博愛特區</p>
                                            <div class="rate_type margin-top-10">
                                                <div class="star-rating" title="Rated 4.50 out of 5">
                                                    <span style="width: 90%">4.50 out of 5</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End OWL Slider -->
                            <!-- ======== 旅行明信片 ======== -->
                            <div class="widget widget_search ">
                                <div class="widget_title text_center">
                                    <img src="img-main/title_postcard.jpg" alt="" />
                                </div>
                            </div>
                            <!-- OWL Slider -->
                            <div id="" class="postcard_owl_slider item_slider owl-carousel owl-theme">
                                <div class="item padding_5">
                                    <div class="medium_article_list">
                                        <div class="thumb_wrapper">
                                            <a href="#" class="" title="">
                                                <img src="demo/banners/demo750x450-7.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="content_wrapper padding_5">
                                            <h5><a href="#">旅行明信片標題旅行明信片標題旅行明信片標題</a></h5>
                                            <div>
                                                <i class="fa fa-map-marker"></i>
                                                <span class="text_grey">高雄市 博愛特區</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item padding_5">
                                    <div class="medium_article_list">
                                        <div class="thumb_wrapper">
                                            <a href="#" class="" title="">
                                                <img src="demo/banners/demo750x450-8.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="content_wrapper padding_5">
                                            <h5><a href="#">旅行明信片標題旅行明信片標題旅行明信片標題</a></h5>
                                            <div>
                                                <i class="fa fa-map-marker"></i>
                                                <span class="text_grey">高雄市 博愛特區</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item padding_5">
                                    <div class="medium_article_list">
                                        <div class="thumb_wrapper">
                                            <a href="#" class="" title="">
                                                <img src="demo/banners/demo750x450-9.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="content_wrapper padding_5">
                                            <h5><a href="#">旅行明信片標題旅行明信片標題旅行明信片標題</a></h5>
                                            <div>
                                                <i class="fa fa-map-marker"></i>
                                                <span class="text_grey">高雄市 博愛特區</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item padding_5">
                                    <div class="medium_article_list">
                                        <div class="thumb_wrapper">
                                            <a href="#" class="" title="">
                                                <img src="demo/banners/demo750x450-7.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="content_wrapper padding_5">
                                            <h5><a href="#">旅行明信片標題旅行明信片標題旅行明信片標題</a></h5>
                                            <div>
                                                <i class="fa fa-map-marker"></i>
                                                <span class="text_grey">高雄市 博愛特區</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item padding_5">
                                    <div class="medium_article_list">
                                        <div class="thumb_wrapper">
                                            <a href="#" class="" title="">
                                                <img src="demo/banners/demo750x450-8.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="content_wrapper padding_5">
                                            <h5><a href="#">旅行明信片標題旅行明信片標題旅行明信片標題</a></h5>
                                            <div>
                                                <i class="fa fa-map-marker"></i>
                                                <span class="text_grey">高雄市 博愛特區</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item padding_5">
                                    <div class="medium_article_list">
                                        <div class="thumb_wrapper">
                                            <a href="#" class="" title="">
                                                <img src="demo/banners/demo750x450-9.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="content_wrapper padding_5">
                                            <h5><a href="#">旅行明信片標題旅行明信片標題旅行明信片標題</a></h5>
                                            <div>
                                                <i class="fa fa-map-marker"></i>
                                                <span class="text_grey">高雄市 博愛特區</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item padding_5">
                                    <div class="medium_article_list">
                                        <div class="thumb_wrapper">
                                            <a href="#" class="" title="">
                                                <img src="demo/banners/demo750x450-7.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="content_wrapper padding_5">
                                            <h5><a href="#">旅行明信片標題旅行明信片標題旅行明信片標題</a></h5>
                                            <div>
                                                <i class="fa fa-map-marker"></i>
                                                <span class="text_grey">高雄市 博愛特區</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End OWL Slider -->
                        </div>
                        <!-- ======== SIDEBAR ======== -->
                        <div class="col col_4_of_12 sidebar sb_right300 xs_hide">
                            <!-- ======== NEWS ======== -->
                            <div class="breaking_news light border-bottom">
                                <div class="breaking_block">
                                    <div class="item">
                                        <div class="icon"></div>
                                        <span class="text_grey">[2015/09/18]</span>
                                        <br> 最佳借問站站長 票選活動開跑。 快來投下你心目中最親切、最熱 情、最有特色的站長一票吧!!!...
                                    </div>
                                    <div class="item">
                                        <div class="icon"></div>
                                        <span class="text_grey">[2015/09/18]</span>
                                        <br> 最佳借問站站長 票選活動開跑。 快來投下你心目中最親切、最熱 情、最有特色的站長一票吧!!!...
                                    </div>
                                    <div class="item">
                                        <div class="icon"></div>
                                        <span class="text_grey">[2015/09/18]</span>
                                        <br> 最佳借問站站長 票選活動開跑。 快來投下你心目中最親切、最熱 情、最有特色的站長一票吧!!!...
                                    </div>
                                </div>
                            </div>
                            <!-- ======== WIDGET - BANNER 300X250 ======== -->
                            <div class="widget widget_banner_300x600">
                                <div class="banner">
                                    <img src="demo/banners/300x600.jpg" alt="" />
                                </div>
                            </div>
                            <!-- ======== WIDGET - 特別企劃 300X100 ======== -->
                            <div class="widget banner300x100">
                                <div class="widget_title no-border-bottom">
                                    <img src="img-main/title-special.png" alt="" />
                                </div>
                                <div class="banner">
                                    <a href="http://smiletaiwan.cw.com.tw/12summer" target="_blank"><img src="img-main/sp_01.jpg" alt=""></a>
                                    <p>追日。追風。追海的旅行</p>
                                </div>
                                <div class="banner">
                                    <a href="http://smiletaiwan.cw.com.tw/matsufun" target="_blank"><img src="img-main/sp_02.jpg" alt=""></a>
                                    <p>杖起舵兒往前滑！馬祖，等你</p>
                                </div>
                                <div class="banner">
                                    <a href="http://smiletaiwan.cw.com.tw/native" target="_blank"><img src="img-main/sp_03.jpg" alt=""></a>
                                    <p>傾聽部落。你有東西留在我這</p>
                                </div>
                            </div>
                            <!-- ======== WIDGET - BANNER 300X250 ======== -->
                            <div class="widget widget_banner_300x250">
                                <div class="banner">
                                    <script type='text/javascript'>
                                    var googletag = googletag || {};

                                    googletag.cmd = googletag.cmd || [];

                                    (function() {

                                        var gads = document.createElement('script');

                                        gads.async = true;

                                        gads.type = 'text/javascript';

                                        var useSSL = 'https:' == document.location.protocol;

                                        gads.src = (useSSL ? 'https:' : 'http:') +

                                            '//www.googletagservices.com/tag/js/gpt.js';

                                        var node = document.getElementsByTagName('script')[0];

                                        node.parentNode.insertBefore(gads, node);

                                    })();
                                    </script>
                                    <script type='text/javascript'>
                                    googletag.cmd.push(function() {

                                        googletag.defineSlot('/47573522/article_right_300x250(u)', [300, 250], 'div-gpt-ad-1447638783144-0').addService(googletag.pubads());

                                        googletag.pubads().enableSingleRequest();

                                        googletag.enableServices();

                                    });
                                    </script>
                                    <a href="#" target="_blank">
                                        <!-- /47573522/article_right_300x250(u) -->
                                        <div id='div-gpt-ad-1447638783144-0' style='height:250px; width:300px;'>
                                            <script type='text/javascript'>
                                            googletag.cmd.push(function() {
                                                googletag.display('div-gpt-ad-1447638783144-0');
                                            });
                                            </script>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <!-- ======== WIDGET - BANNER 300X250 ======== -->
                            <div class="widget widget_banner_300x250">
                                <div class="banner">
                                    <div id="fb-root"></div>
                                    <script>
                                    (function(d, s, id) {
                                        var js, fjs = d.getElementsByTagName(s)[0];
                                        if (d.getElementById(id)) return;
                                        js = d.createElement(s);
                                        js.id = id;
                                        js.src = "//connect.facebook.net/zh_TW/sdk.js#xfbml=1&version=v2.5&appId=196008494115242";
                                        fjs.parentNode.insertBefore(js, fjs);
                                    }(document, 'script', 'facebook-jssdk'));
                                    </script>
                                    <div class="fb-page" data-href="https://www.facebook.com/smiletaiwan319" data-tabs="timeline" data-width="300" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
                                        <div class="fb-xfbml-parse-ignore">
                                            <blockquote cite="https://www.facebook.com/smiletaiwan319"><a href="https://www.facebook.com/smiletaiwan319">微笑台灣319鄉+</a></blockquote>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <!-- ======== FOOTER ======== -->
            <?php include("footer.php");?>

        </div>
    </div>
    <!--leftmenu-->
    <div class="sb-slidebar sb-left sb-style-overlay">
        <!-- Main Navigation -->
        <?php include("left_menu.php");?>
        <!-- /Main Navigation -->
    </div>
    
    <!--righttmenu-->
    <div class="sb-slidebar sb-right sb-style-overlay">
        <div class="col col_12_of_12 sidebar">
            <!-- ======== WIDGET - BANNER 300X100 ======== -->
            <div class="widget banner300x100 margin-bottom-30">
                <div class="widget_title">
                    <img src="img-main/title-special.png" alt="" />
                </div>
                <div class="banner margin-bottom-30">
                    <a href="http://smiletaiwan.cw.com.tw/12summer" target="_blank"><img src="img-main/sp_01.jpg" alt=""></a>
                    <p>追日。追風。追海的旅行</p>
                </div>
                <div class="banner margin-bottom-30">
                    <a href="http://smiletaiwan.cw.com.tw/matsufun" target="_blank"><img src="img-main/sp_02.jpg" alt=""></a>
                    <p>杖起舵兒往前滑！馬祖，等你</p>
                </div>
                <div class="banner margin-bottom-30">
                    <a href="http://smiletaiwan.cw.com.tw/native" target="_blank"><img src="img-main/sp_03.jpg" alt=""></a>
                    <p>傾聽部落。你有東西留在我這</p>
                </div>
            </div>
            <!-- ======== WIDGET - 駐站旅人 ======== -->
            <div class="widget widget_socialize margin-bottom-20">
                <div class="widget_title no-border-bottom">
                    <img src="img-main/title-traveler.png" alt="" />
                </div>
                <div class="row">
                    <div class="col_4_of_12 alignleft">
                        <div class="writer-single">
                            <img src="img-main/person_pic_default.jpg" alt="">
                            <p>作者名稱</p>
                        </div>
                    </div>
                    <div class="col_4_of_12 alignleft">
                        <div class="writer-single">
                            <img src="img-main/person_pic_default.jpg" alt="">
                            <p>作者名稱</p>
                        </div>
                    </div>
                    <div class="col_4_of_12 alignleft">
                        <div class="writer-single">
                            <img src="img-main/person_pic_default.jpg" alt="">
                            <p>作者名稱</p>
                        </div>
                    </div>
                    <div class="col_4_of_12 alignleft">
                        <div class="writer-single">
                            <img src="img-main/person_pic_default.jpg" alt="">
                            <p>作者名稱</p>
                        </div>
                    </div>
                    <div class="col_4_of_12 alignleft">
                        <div class="writer-single">
                            <img src="img-main/person_pic_default.jpg" alt="">
                            <p>作者名稱</p>
                        </div>
                    </div>
                    <div class="col_4_of_12 alignleft">
                        <div class="writer-single">
                            <img src="img-main/person_pic_default.jpg" alt="">
                            <p>作者名稱</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ======== WIDGET - BANNER 300X250 ======== -->
            <div class="widget widget_banner_300x250 margin-bottom-20">
                <div class="banner">
                    <script type='text/javascript'>
                    var googletag = googletag || {};

                    googletag.cmd = googletag.cmd || [];

                    (function() {

                        var gads = document.createElement('script');

                        gads.async = true;

                        gads.type = 'text/javascript';

                        var useSSL = 'https:' == document.location.protocol;

                        gads.src = (useSSL ? 'https:' : 'http:') +

                            '//www.googletagservices.com/tag/js/gpt.js';

                        var node = document.getElementsByTagName('script')[0];

                        node.parentNode.insertBefore(gads, node);

                    })();
                    </script>
                    <script type='text/javascript'>
                    googletag.cmd.push(function() {

                        googletag.defineSlot('/47573522/article_right_300x250(u)', [300, 250], 'div-gpt-ad-1447638783144-0').addService(googletag.pubads());

                        googletag.pubads().enableSingleRequest();

                        googletag.enableServices();

                    });
                    </script>
                    <a href="#" target="_blank">
                        <!-- /47573522/article_right_300x250(u) -->
                        <div id='div-gpt-ad-1447638783144-0' style='height:250px; width:300px;'>
                            <script type='text/javascript'>
                            googletag.cmd.push(function() {
                                googletag.display('div-gpt-ad-1447638783144-0');
                            });
                            </script>
                        </div>
                    </a>
                </div>
            </div>
            <!-- ======== WIDGET - LATEST POSTS ======== -->
            <div class="widget widget_latest_posts margin-bottom-20">
                <div class="widget_title no-border-bottom">
                    <img src="img-main/title_top.png" alt="" />
                </div>
                <div class="item">
                    <div class="item_header">
                        <i class="fa fa-3x fa-angle-right text_grey"></i>
                    </div>
                    <div class="item_wrapper">
                        <div class="item_content">
                            <h4><a #href="#" target="_blank">【新竹 旅遊】客家湯圓節健走。傳統技藝新玩法</a></h4>
                            <p>哪哪麻這天來到竹東火車站參加湯圓節的活動, 話說竹東每年都會辦一次湯圓節, 有園遊會賣當地特色產品 ...</p>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="item_header">
                        <i class="fa fa-3x fa-angle-right text_grey"></i>
                    </div>
                    <div class="item_wrapper">
                        <div class="item_content">
                            <h4><a #href="#" target="_blank">當老師問我……</a></h4>
                            <p>當老師問我： 「耶誕節都怎樣慶祝？」當我的外籍老師問我時，直覺地脫口而出，「這個啊，我們都過農曆年的。」 老師很有興致，...</p>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="item_header">
                        <i class="fa fa-3x fa-angle-right text_grey"></i>
                    </div>
                    <div class="item_wrapper">
                        <div class="item_content">
                            <h4><a #href="#" target="_blank">台中大雪山．步道散步吸收芬多精 & 雪山神木 & 天</a></h4>
                            <p>炎熱的夏天，是不是很想上山消暑呢？鳥夫人：不是~~~~ 呵呵，因為鳥夫人喜歡大海不喜歡山，但鳥先生跟鳥夫人相反，所以兩個人...</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ======== WIDGET - BANNER 300X250 ======== -->
            <div class="widget widget_banner_300x250">
                <div class="banner margin-bottom-30">
                    <script type='text/javascript'>
                    var googletag = googletag || {};

                    googletag.cmd = googletag.cmd || [];

                    (function() {

                        var gads = document.createElement('script');

                        gads.async = true;

                        gads.type = 'text/javascript';

                        var useSSL = 'https:' == document.location.protocol;

                        gads.src = (useSSL ? 'https:' : 'http:') +

                            '//www.googletagservices.com/tag/js/gpt.js';

                        var node = document.getElementsByTagName('script')[0];

                        node.parentNode.insertBefore(gads, node);

                    })();
                    </script>
                    <script type='text/javascript'>
                    googletag.cmd.push(function() {

                        googletag.defineSlot('/47573522/article_right_300x250(d)', [300, 250], 'div-gpt-ad-1447638832673-0').addService(googletag.pubads());

                        googletag.pubads().enableSingleRequest();

                        googletag.enableServices();

                    });
                    </script>
                    <a href="#" target="_blank">
                        <!-- /47573522/article_right_300x250(d) -->
                        <div id='div-gpt-ad-1447638832673-0' style='height:250px; width:300px;'>
                            <script type='text/javascript'>
                            googletag.cmd.push(function() {
                                googletag.display('div-gpt-ad-1447638832673-0');
                            });
                            </script>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!-- ======== JAVASCRIPTS ======== -->
    <script src="js/jquery.min.js"></script>
    <script src="js/jquery.ui.min.js"></script>
    <script src="js/jquery.fitvids.js"></script>
    <script src="js/jquery.owlcarousel.min.js"></script>
    <script src="js/jquery.magnific.popup.min.js"></script>
    <script src="js/jquery.parallax.min.js"></script>
    <script src="js/jquery.smooth.scroll.js"></script>
    <script src="js/jquery.init.js"></script>
    <script src="sys_plugin/oki_zippicker/oki_zipcode_mem.js"></script>
    <!-- MyWeather JS (needed) -->
    <script src="3dParty/MyWeather/js/MyWeather.js"></script>
    <!-- owl-carousel -->
    <script>
    $(document).ready(function() {
        $("#owl-demo").owlCarousel({
            navigation: false, // Show next and prev buttons
            slideSpeed: 300,
            paginationSpeed: 400,
            singleItem: true,
            autoPlay: true

            // "singleItem:true" is a shortcut for:
            // items : 1, 
            // itemsDesktop : false,
            // itemsDesktopSmall : false,
            // itemsTablet: false,
            // itemsMobile : false
        });
    });
    $('#t_owl_slider').owlCarousel({
        navigation: false, // Show next and prev buttons
        slideSpeed: 300,
        paginationSpeed: 400,
        singleItem: true,
        autoPlay: true,
    })

    $('.shop_owl_slider').owlCarousel({
        navigation: true, // Show next and prev buttons
        pagination: false,
        slideSpeed: 300,
        paginationSpeed: 400,
        autoPlay: true,
        loop: true,
        margin: 10,
        items: 3,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                nav: true
            },
            468: {
                items: 2,
                nav: false
            },
            989: {
                items: 3,
                nav: true,
                loop: false
            }
        }
    })
    $('.postcard_owl_slider').owlCarousel({
        navigation: true, // Show next and prev buttons
        pagination: false,
        slideSpeed: 300,
        paginationSpeed: 400,
        autoPlay: true,
        loop: true,
        margin: 10,
        items: 3,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                nav: true
            },
            468: {
                items: 2,
                nav: false
            },
            989: {
                items: 3,
                nav: true,
                loop: false
            }
        }
    })
    </script>
    <!-- Slidebars -->
    <script src="3dParty/slidebars/slidebars.min.js"></script>
    <script>
    (function($) {
        $(document).ready(function() {
            $.slidebars();

            $("#schbtn").on("click", function() {
                $("#schform").submit()
            });

        });
    })(jQuery);
    </script>
    <!-- weather -->
    <script type="text/javascript">
    $.MyWeather({
            elementid: "#localinfo",
            position: "right",
            showpopup: true,
            showinfo: true,
            temperature: "c",
            closeicon: false,
            showicon: true,
            showtemperature: true,
            showlocation: true,
            showip: false,
            size: 80,
            iconcolor: "#ffffff",
            fontcolor: "#ffffff",
        },
        function(City, Country, IP, Latitude, Longitude, Temperature) {
            // This callback function is called when we get those values. The callback is optional too.
        });
    </script>
</body>

</html>
<script>