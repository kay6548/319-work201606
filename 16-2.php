<!DOCTYPE html>
<html  lang="zh-Hant-TW">
    <head>
        <title>319鄉鎮</title>
        <!-- ======== META TAGS ======== -->
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <!-- ======== FAVICONS ======== -->
        <link rel="icon" href="favicon.ico">
        <link rel="apple-touch-icon" href="favicon.png">
        <!-- ======== STYLESHEETS ======== -->
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/typography.css">
        <link rel="stylesheet" href="css/fontawesome.css">
        <link rel="stylesheet" href="css/popup.css">
        <link rel="stylesheet" href="css/owlslider.css">
        <link rel="stylesheet" href="css/style.css">
        <link id="switch_style" rel="stylesheet" href="demo/main-color/blue.css">
        <!-- Slidebars CSS -->
        <link rel="stylesheet" href="3dParty/slidebars/slidebars.min.css">
        <!-- ======== RESPONSIVE ======== -->
        <link rel="stylesheet" media="(max-width:479px)" href="css/responsive-small.css">
        <link rel="stylesheet" media="(min-width:480px) and (max-width:768px)" href="css/responsive-0.css">
        <link rel="stylesheet" media="(min-width:769px) and (max-width:992px)" href="css/responsive-768.css">
        <link rel="stylesheet" media="(min-width:993px) and (max-width:1200px)" href="css/responsive-992.css">
        <link rel="stylesheet" media="(min-width:1201px)" href="css/responsive-1200.css">
        
        <!-- Slidebars CSS -->
        <link rel="stylesheet" href="3dParty/owl-carousel/owl.carousel.css">
        <link rel="stylesheet" href="3dParty/owl-carousel/owl.theme.css">

        <!-- MyWeather CSS (needed) -->
        <link rel="stylesheet" type="text/css" href="3dParty/MyWeather/css/MyWeather.css">
        <link rel="stylesheet" type="text/css" href="3dParty/bootstrap/css/zbootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="3dParty/bootstrap/css/bootstrap-theme.min.css">
        <link rel="stylesheet" type="text/css" href="css/zonestyle.css">



        <!-- ======== GOOGLE FONTS ======== -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Titillium+Web:400,300,300italic,400italic,700,700italic,600italic,600">
        <link href='https://fonts.googleapis.com/css?family=PT+Sans:400italic,400|Raleway:200italic,300,300italic|Oxygen:300' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Ubuntu:400,300,300italic' rel='stylesheet' type='text/css'>
        <!-- ======== GOOGLE ANALYTICS ======== -->
    <style>
    .owl-carousel{ display:block !important}

    #owl-demo .item img{
        display: block;
        width: 100%;
        height: auto;
    }


    </style>
    </head>
    <body>
        
        <!-- ======== WRAPPER ======== -->
        <div id="wrapper " class="wide">
        <div id="sb-site">
            <!-- ======== BANNER ======== -->
            <div class="top-banner728x90 text_center aligncenter bg_grey">
				<script type='text/javascript'>
                
                  var googletag = googletag || {};
                
                  googletag.cmd = googletag.cmd || [];
                
                  (function() {
                
                    var gads = document.createElement('script');
                
                    gads.async = true;
                
                    gads.type = 'text/javascript';
                
                    var useSSL = 'https:' == document.location.protocol;
                
                    gads.src = (useSSL ? 'https:' : 'http:') +
                
                      '//www.googletagservices.com/tag/js/gpt.js';
                
                    var node = document.getElementsByTagName('script')[0];
                
                    node.parentNode.insertBefore(gads, node);
                
                  })();
                
                </script>
                <script type='text/javascript'>
                
                  googletag.cmd.push(function() {
                
                    googletag.defineSlot('/47573522/travel_down_728x90', [728, 90], 'div-gpt-ad-1447638964347-0').addService(googletag.pubads());
                
                    googletag.pubads().enableSingleRequest();
                
                    googletag.enableServices();
                
                  });
                
                </script>            
                <a href="#" target="_blank">
                <!-- /47573522/travel_down_728x90 -->
                
                <div id='div-gpt-ad-1447638964347-0' style='height:90px; width:728px; margin:0 auto;'>
                
                <script type='text/javascript'>
                
                googletag.cmd.push(function() { googletag.display('div-gpt-ad-1447638964347-0'); });
                
                </script>
                
                </div>
                </a>
            </div>
            <!-- ======== HEADER ======== -->
            <?php include("header_menu.php");?>
            
            <!-- ======== Slider CONTAINER ======== -->
            <div class="fullwidth bg_grey">
                <div class="container">
                    <ul class="breadcrumb">
                      <li><a href="#">首頁</a></li>
                      <li><a href="#">單元名稱</a></li>
                      <li>頁面名稱</li>
                    </ul> 
                </div> 
            </div>
            
            
            <!-- ======== SECTION ======== -->
            <section id="page_wrapper">
                <div class="container">
                    <div class="row">
                        <!-- ======== MAIN CONTENT ======== -->
                        <div class="col col_8_of_12 main_content">
                                  
                                  <div class="z_bluebk z_lineheigh30 z_fontcwhile z_padding10 z_margintop30">
                                              在地任務
                                  </div>
                                  
                                  <div class="z_margintop10">
               		  		      <img src="img-main/postcard02.jpg" width="100%"> 
                                  </div>
                                  <div class="clearfix"></div>
                                  <div style="border-bottom:1px solid #ccc; height:1px; margin-top:20px;"></div>
                                  <div class="row">
                                        <div class="col-sm-2 col-xs-3 z_fontc333" style="margin-right:-15px;">任務名稱：</div><div class="col-sm-9 col-xs-9 z_fontc666" style="margin-left:-15px;">台北市/信義區/大湖草莓產銷班</div>
                                        <div class="clearfix"></div>
                                        <div class="col-sm-2 col-xs-3 z_fontc333" style="margin-right:-15px;">任務說明：</div><div class="col-sm-10 z_fontc666" style="margin-left:-15px;">特色景點</div>
                                        <div class="clearfix"></div>
                                        <!--<div class="col-sm-2 col-xs-3 z_fontc333" style="margin-right:-15px;">徽章圖片：</div><div class="col-sm-9 col-xs-9 z_fontc666" style="margin-left:-15px;">苗栗縣、大湖</div>
                                        <div class="clearfix"></div>-->
                                        <div class="col-sm-2 col-xs-3 z_fontc333" style="margin-right:-15px;">開始時間：</div><div class="col-sm-10 col-xs-9 z_fontc666" style="margin-left:-15px;">苗栗大湖鄉義和村蔗廍坪7之1號
     (石屋草莓園)</div>
                                        <div class="clearfix"></div>	
                                        <div class="col-sm-2 col-xs-3 z_fontc333" style="margin-right:-15px;">結束時間：</div><div class="col-sm-10 col-xs-9 z_fontc666" style="margin-left:-15px;">0919703584</div>
                                        <div class="clearfix"></div>
                                        <div class="col-sm-2 col-xs-3 z_fontc333" style="margin-right:-15px;">達成百分比：</div><div class="col-sm-9 col-xs-9 z_fontc666" style="margin-left:-15px;">0700-1800</div>
                                        <div class="clearfix"></div>
                                         
                                  </div>
                                  
                                  <div class="z_margintop10 z_border_cc">
                               	    	<img src="img-main/banner728x90.jpg" class="visible-lg visible-sm visible-md"> 
                                        <img src="demo/banners/320X100.jpg" class="visible-xs">
                                  </div>
                                  <div class="clearfix"></div>
                                  <div class="z_bluebk z_lineheigh30 z_fontcwhile z_padding10 z_margintop30">
                                              任務指定聯盟
                                  </div>
                                  <div class="clearfix"></div>
                                  <div class="z_margintop10">
                                  			  <!-- OWL Slider -->
												  <div id="" class="shop_owl_slider item_slider owl-carousel owl-theme">
													  
													  <?php for($v=0;$v<5;$v++){?>
													  <div class="item padding_5">
														    <?php for($i=0;$i<3;$i++){?>
															<div class="medium_article_list">
															   <div class="thumb_wrapper">
																	  <div class="type_box">
																		  <div class="type">食</div>
																	  </div>
																	<a href="#" class="" title="">
																		<img src="demo/banners/demo750x450-4.jpg" alt="">
																	</a>
																</div>
																<div class="content_wrapper padding_5">
																	<h5><a href="#">在地聯盟店家名稱</a></h5>
																	<p class="text_grey">高雄市 博愛特區</p>
																	<div class="rate_type margin-top-10">
																	  <div class="star-rating" title="Rated 4.50 out of 5">
																		  <span style="width: 90%">4.50 out of 5</span>
																	  </div>
																  </div>
																</div>
															</div>
															<?php }?>
													  </div>
													  <?php }?>
												  </div>
											  <!-- End OWL Slider -->
                                  
                                  </div>
                                                                    
                                 
                                  
                                  
                       </div>
                        <!-- ======== SIDEBAR ======== -->
                        <div class="col col_4_of_12 sidebar sb_right300 xs_hide">
                                
                            <?php include("right2button.php");?>
                            
                      </div>
                    </div>
                </div>
            </section>
            <!-- ======== FOOTER ======== -->
            <?php include("footer.php");?>

</div>
        </div>
        
        <!--leftmenu-->
    <div class="sb-slidebar sb-left sb-style-overlay">
        <!-- Main Navigation -->
        <?php include("left_menu.php");?>
        <!-- /Main Navigation -->
    </div>
        <!--righttmenu-->
		<div class="sb-slidebar sb-right sb-style-overlay">
          <div class="col col_12_of_12 sidebar">
       			<?php include("right2button.php");?>
          </div>
        </div>
        
        
        
        <!-- ======== JAVASCRIPTS ======== -->
        <script src="js/jquery.min.js"></script>
        <script src="js/jquery.ui.min.js"></script>
        <script src="js/jquery.fitvids.js"></script>
        <script src="js/jquery.owlcarousel.min.js"></script>
        <script src="js/jquery.magnific.popup.min.js"></script>
        <script src="js/jquery.parallax.min.js"></script>
        <script src="js/jquery.smooth.scroll.js"></script>
        <script src="js/jquery.init.js"></script>
         <script src="sys_plugin/oki_zippicker/oki_zipcode_mem.js"></script>
  <!-- MyWeather JS (needed) -->
  <script src="3dParty/MyWeather/js/MyWeather.js"></script>
  <script src="3dParty/bootstrap/js/bootstrap.minzone.js"></script>
       
        
<!-- owl-carousel -->
    <script>
    $(document).ready(function() {
      $("#owl-demo").owlCarousel({
          navigation : false, // Show next and prev buttons
          slideSpeed : 300,
          paginationSpeed : 400,
          singleItem:true,
      autoPlay : true
     
          // "singleItem:true" is a shortcut for:
          // items : 1, 
          // itemsDesktop : false,
          // itemsDesktopSmall : false,
          // itemsTablet: false,
          // itemsMobile : false
      });
	  $('#myTab a').click(function(e) {
                e.preventDefault()
                $(this).tab('show')
            })
    });
  $('#t_owl_slider').owlCarousel({
          navigation : false, // Show next and prev buttons
          slideSpeed : 300,
          paginationSpeed : 400,
          singleItem:true,
      autoPlay : true,
})

 	$('.shop_owl_slider').owlCarousel({
          navigation : true, // Show next and prev buttons
          pagination : false,
          slideSpeed : 300,
          paginationSpeed : 400,
          autoPlay : true,
			loop:true,
				margin:10,
				items : 3, 
				responsiveClass:true,
				responsive:{
					0:{
						items:1,
						nav:true
					},
					468:{
						items:2,
						nav:false
					},
					989:{
						items:3,
						nav:true,
						loop:false
					}
				}
			})
  $('.postcard_owl_slider').owlCarousel({
          navigation : true, // Show next and prev buttons
          pagination : false,
          slideSpeed : 300,
          paginationSpeed : 400,
          autoPlay : true,
loop:true,
    margin:10,
    items : 3, 
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        468:{
            items:2,
            nav:false
        },
        989:{
            items:3,
            nav:true,
            loop:false
        }
    }
})
$('.postcard_owl_slider2').owlCarousel({
          navigation : true, // Show next and prev buttons
          pagination : false,
          slideSpeed : 300,
          paginationSpeed : 400,
          autoPlay : true,
loop:true,
    margin:10,
    items : 2, 
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        468:{
            items:2,
            nav:false
        },
        989:{
            items:3,
            nav:true,
            loop:false
        }
    }
})
    </script>
    
<!-- Slidebars -->
<script src="3dParty/slidebars/slidebars.min.js"></script>
<script>
    (function($) {
        $(document).ready(function() {
            $.slidebars();
      
      $("#schbtn").on("click",function(){ $("#schform").submit() });
      
        });
    }) (jQuery);
</script>

<!-- weather -->
<script type="text/javascript">

 $.MyWeather({
           elementid: "#localinfo",
           position: "right",
           showpopup: true,
           showinfo: true,
           temperature: "c",
           closeicon: false,
           showicon: true,
           showtemperature: true,
           showlocation: true,
           showip: false,
           size: 80,
           iconcolor: "#ffffff",
           fontcolor: "#ffffff",
},
function(City,Country, IP, Latitude, Longitude, Temperature){
// This callback function is called when we get those values. The callback is optional too.
}); 


</script> 

    </body>
</html>
<script>