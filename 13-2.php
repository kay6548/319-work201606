<!DOCTYPE html>
<html  lang="zh-Hant-TW">
    <head>
        <title>319鄉鎮</title>
        <!-- ======== META TAGS ======== -->
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <!-- ======== FAVICONS ======== -->
        <link rel="icon" href="favicon.ico">
        <link rel="apple-touch-icon" href="favicon.png">
        <!-- ======== STYLESHEETS ======== -->
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/typography.css">
        <link rel="stylesheet" href="css/fontawesome.css">
        <link rel="stylesheet" href="css/popup.css">
        <link rel="stylesheet" href="css/owlslider.css">
        <link rel="stylesheet" href="css/style.css">
        <link id="switch_style" rel="stylesheet" href="demo/main-color/blue.css">
        <!-- Slidebars CSS -->
        <link rel="stylesheet" href="3dParty/slidebars/slidebars.min.css">
        <!-- ======== RESPONSIVE ======== -->
        <link rel="stylesheet" media="(max-width:479px)" href="css/responsive-small.css">
        <link rel="stylesheet" media="(min-width:480px) and (max-width:768px)" href="css/responsive-0.css">
        <link rel="stylesheet" media="(min-width:769px) and (max-width:992px)" href="css/responsive-768.css">
        <link rel="stylesheet" media="(min-width:993px) and (max-width:1200px)" href="css/responsive-992.css">
        <link rel="stylesheet" media="(min-width:1201px)" href="css/responsive-1200.css">
        
        <!-- Slidebars CSS -->
        <link rel="stylesheet" href="3dParty/owl-carousel/owl.carousel.css">
        <link rel="stylesheet" href="3dParty/owl-carousel/owl.theme.css">

        <!-- MyWeather CSS (needed) -->
        <link rel="stylesheet" type="text/css" href="3dParty/MyWeather/css/MyWeather.css">
        <link rel="stylesheet" type="text/css" href="3dParty/bootstrap/css/zbootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="3dParty/bootstrap/css/bootstrap-theme.min.css">
        <link rel="stylesheet" type="text/css" href="css/zonestyle.css">



        <!-- ======== GOOGLE FONTS ======== -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Titillium+Web:400,300,300italic,400italic,700,700italic,600italic,600">
        <link href='https://fonts.googleapis.com/css?family=PT+Sans:400italic,400|Raleway:200italic,300,300italic|Oxygen:300' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Ubuntu:400,300,300italic' rel='stylesheet' type='text/css'>
        <!-- ======== GOOGLE ANALYTICS ======== -->
    <style>
    .owl-carousel{ display:block !important}

    #owl-demo .item img{
        display: block;
        width: 100%;
        height: auto;
    }


    </style>
    </head>
    <body>
        
        <!-- ======== WRAPPER ======== -->
        <div id="wrapper " class="wide">
        <div id="sb-site">
            <!-- ======== BANNER ======== -->
            <div class="top-banner728x90 text_center aligncenter bg_grey">
				<script type='text/javascript'>
                
                  var googletag = googletag || {};
                
                  googletag.cmd = googletag.cmd || [];
                
                  (function() {
                
                    var gads = document.createElement('script');
                
                    gads.async = true;
                
                    gads.type = 'text/javascript';
                
                    var useSSL = 'https:' == document.location.protocol;
                
                    gads.src = (useSSL ? 'https:' : 'http:') +
                
                      '//www.googletagservices.com/tag/js/gpt.js';
                
                    var node = document.getElementsByTagName('script')[0];
                
                    node.parentNode.insertBefore(gads, node);
                
                  })();
                
                </script>
                <script type='text/javascript'>
                
                  googletag.cmd.push(function() {
                
                    googletag.defineSlot('/47573522/travel_down_728x90', [728, 90], 'div-gpt-ad-1447638964347-0').addService(googletag.pubads());
                
                    googletag.pubads().enableSingleRequest();
                
                    googletag.enableServices();
                
                  });
                
                </script>            
                <a href="#" target="_blank">
                <!-- /47573522/travel_down_728x90 -->
                
                <div id='div-gpt-ad-1447638964347-0' style='height:90px; width:728px; margin:0 auto;'>
                
                <script type='text/javascript'>
                
                googletag.cmd.push(function() { googletag.display('div-gpt-ad-1447638964347-0'); });
                
                </script>
                
                </div>
                </a>
            </div>
            <!-- ======== HEADER ======== -->
            <?php include("header_menu.php");?>
            
            <!-- ======== Slider CONTAINER ======== -->
            <div class="fullwidth bg_grey">
                <div class="container">
                    <ul class="breadcrumb">
                      <li><a href="#">首頁</a></li>
                      <li><a href="#">單元名稱</a></li>
                      <li>頁面名稱</li>
                    </ul> 
                </div> 
            </div>
            
            
            <!-- ======== SECTION ======== -->
            <section id="page_wrapper">
                <div class="container">
                    <div class="row">
                        <!-- ======== MAIN CONTENT ======== -->
                        <div class="col col_8_of_12 main_content">
                                  <div class="article_list standard no_paddong z_margintop10"> 
                                       <div class="z_bluebk z_lineheigh30 z_fontcwhile z_padding10 zone-bottom-20">
                                      店家名稱：標題標題
                                  </div>
                                      <div id="owl-demo" class="owl-carousel owl-theme ">
                                            <div class="item">
                                               
                                                <div class="item_header">
                                                    <a href="#">
                                                        <img src="demo/banners/demo750x450-1.jpg" alt="">
                                                    </a>
                                                </div>                                                    
                                            </div>
                                            <div class="item">
                                                <div class="item_header">
                                                    <a href="#">
                                                        <img src="demo/banners/demo750x450-2.jpg" alt="">
                                                    </a>
                                                </div> 
                                            </div>
                                            <div class="item">
                                                <div class="item_header">
                                                    <a href="#">
                                                        <img src="demo/banners/demo750x450-3.jpg" alt="">
                                                    </a>
                                                </div> 
                                            </div>
                                            <div class="item">
                                                <div class="item_header">
                                                    <a href="#">
                                                        <img src="demo/banners/demo750x450-4.jpg" alt="">
                                                    </a>
                                                </div> 
                                            </div>
                                            <div class="item">
                                                <div class="item_header">
                                                    <a href="#">
                                                        <img src="demo/banners/demo750x450-5.jpg" alt="">
                                                    </a>
                                                </div> 
                                            </div>
                                      </div>
                                  </div>
                                  
                                  <div>
                                  		<div class="z_more_btn3  z_bluebk"><a href="#" class="z_more_gray z_bluebk">我要評分</a></div>
                                        <div class="z_more_btn3">
                                           <div class="star-rating z_more_img" title="Rated 3.5 out of 5">
                                              <span style="width: 90%;">2 out of 2</span>
                                           </div>
                                        </div> 
                                        <!--<div class="z_more_btn3"><a href="#" class="z_more_a">聯盟故事</a></div>-->                                 		
                                  </div>
                                  <div class="clearfix"></div>
                                  <div style="border-bottom:1px solid #ccc; height:1px; margin-top:10px;"></div>
                                  <div class="row">
                                  	<div class="col-sm-7">
                                          <div class="row">
                                                <div class="col-sm-3 col-xs-3 z_fontc333" style="margin-right:-15px;">聯盟名稱：</div><div class="col-sm-9 col-xs-9 z_fontc666" style="margin-left:-15px;">台北市/信義區/大湖草產銷班</div>
                                                <div class="clearfix"></div>
                                                <div class="col-sm-3 col-xs-3 z_fontc333" style="margin-right:-15px;">類&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;別：</div><div class="col-sm-9 z_fontc666" style="margin-left:-15px;">特色景點</div>
                                                <div class="clearfix"></div>
                                                <div class="col-sm-3 col-xs-3 z_fontc333" style="margin-right:-15px;">縣市鄉鎮：</div><div class="col-sm-9 col-xs-9 z_fontc666" style="margin-left:-15px;">苗栗縣、大湖</div>
                                                <div class="clearfix"></div>
                                                <div class="col-sm-3 col-xs-3 z_fontc333" style="margin-right:-15px;">地&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;址：</div><div class="col-sm-9 col-xs-9 z_fontc666" style="margin-left:-15px;">苗栗大湖鄉義和村蔗廍坪7之1號
             (石屋草莓園)</div>
                                                <div class="clearfix"></div>	
                                                <div class="col-sm-3 col-xs-3 z_fontc333" style="margin-right:-15px;">電&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;話：</div><div class="col-sm-9 col-xs-9 z_fontc666" style="margin-left:-15px;">0919703584</div>
                                                <div class="clearfix"></div>
                                                <div class="col-sm-3 col-xs-3 z_fontc333" style="margin-right:-15px;">營業時間：</div><div class="col-sm-9 col-xs-9 z_fontc666" style="margin-left:-15px;">0700-1800</div>
                                                <div class="clearfix"></div>
                                                 <div class="col-sm-3 col-xs-3 z_fontc333" style="margin-right:-15px;">價&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;格：</div><div class="col-sm-9 col-xs-9 z_fontc666" style="margin-left:-15px;">大湖草莓產銷班 規定時價</div>
                                                <div class="clearfix"></div>
                                                 <div class="col-sm-3 col-xs-3 z_fontc333" style="margin-right:-15px;">網&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;址：</div><div class="col-sm-9 col-xs-9 z_fontc666" style="margin-left:-15px;">http://dahu.pe.hu/</div>
                                                <div class="clearfix"></div>
                                          </div>
                                    </div>
                                    <div class="col-sm-5">
                                        <div class="z_margintop10">
                                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3615.0156311463666!2d121.56191976563264!3d25.033543594462344!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3442abb6da80a7ad%3A0xacc4d11dc963103c!2z6Ie65YyXMTAx6LO854mp5Lit5b-D!5e0!3m2!1szh-TW!2stw!4v1461424560439" width="100%" height="230" frameborder="0" style="border:0" allowfullscreen></iframe>
                                        </div>
                                    </div> 
                                    
                                    <div class="clearfix"></div> 
                                    <div class="col-sm-2 col-xs-12 z_fontc333" style="margin-right:-15px;"> 簡&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;介：</div><div class="col-sm-10 col-xs-12 z_fontc666 z_-marginleft30">大湖草莓季於每年的12月翌年的４月為草莓盛產期， 草莓觀光果園
             最著名的地方就在苗栗縣的大湖鄉， 草莓季走進大湖，空氣裡就飄
             滿著草莓酸甜的香氣， 親手採草莓這裡少了喧鬧的氣息，卻多了舒
             適慵懶田園風光， 那種滿足的感覺是直接在市場裡頭買現成是無法
             體會到的唷！ 石屋草莓園農場座落於 大湖鄉石雲道 石屋段 半山頂
             平台農場 空氣清新、遠離塵囂、風景宜人、草莓香甜 果園主人每天
             採用 蔡18菌培養液, em菌 獨特菌種栽培法 細心的照顧 使得石屋
             草莓園 能種出又香又甜的草莓。 12月至04月都有許多鮮紅的草莓
             可採收。 讓品嘗過草莓的客人都讚不絕口， 附近名勝景點有 法雲
             寺、萬聖宮、雪霸國家公園管理處、 汶水老街，馬拉邦山、大湖溫
             泉等，石雲宮瞭望台 都是週休假日休閒旅遊踏青、遠離塵囂的好去
             處。</div>
                                                <div class="clearfix"></div>
                                    <div class="col-sm-2 col-xs-12 z_fontc333" style="margin-right:-15px;">交通資訊：</div><div class="col-sm-10 col-xs-12 z_fontc666 z_-marginleft30">交通工具 南下路線： →經由國道1號下苗栗交流道→右轉台6號公
             路並直行約800公尺→左轉上72號東西向快速道路→ 至終點汶水右
             轉接台3公路→至134.88KM處，南湖國小(天橋)→右轉 石雲道直
             行→ 石雲道(石屋段)→即達本農場(石屋草莓園) 北上路線： →經
             由國道1號→透過豐原系統交流道接國道4號→左轉台3現經豐原、
             石岡、東勢、卓蘭→ 至134.88KM處，南湖國小(天橋)→右轉 石
             雲道直行→石雲道(石屋段)→即達本農場(石屋草莓園) 搭乘公共汽
             車： 苗栗火車站前搭乘新竹客運到大湖「南湖國小站」下車 → 南
             湖天橋右轉石雲道直行1.4公里→即達 石屋草莓園。</div>
                                                <div class="clearfix"></div>
                                                <div class="col-sm-2 col-xs-12 z_fontc333" style="margin-right:-15px;">到店打卡優惠：</div><div class="col-sm-10 col-xs-12 z_fontc666 z_-marginleft20">草莓季期間 微笑聯盟FB打卡優惠。</div>
                                                <div class="clearfix"></div>
                                                            
                                  </div>
                                   <div class="z_margintop10 z_border_cc">
                               	    	<img src="img-main/banner728x90.jpg" class="visible-lg visible-sm visible-md"> 
                                        <img src="demo/banners/320X100.jpg" class="visible-xs">
                                    </div>
                                  <div class="clearfix"></div>
                                  <div class="z_bluebk z_lineheigh30 z_fontcwhile z_padding10 z_margintop30">
                                      最活躍微笑聯盟
                                  </div>
                                  <div class="z_margintop10">
                                  			 
                                              <!-- OWL Slider -->
                                                  <div id="" class="shop_owl_slider item_slider owl-carousel owl-theme">
                                  <div class="item padding_5">
                                      <div class="medium_article_list">
                                          <div class="thumb_wrapper">
                                                  <div class="type_box">
                                                      <div class="type">食</div>
                                                  </div>
                                                <a href="#" class="" title="">
                                                    <img src="demo/banners/demo750x450-4.jpg" alt="">
                                                </a>
                                          </div>
                                            <div class="content_wrapper padding_5">
                                                <h5><a href="#">在地聯盟店家名稱</a></h5>
                                                <p class="text_grey">高雄市 博愛特區</p>
                                                <div class="rate_type margin-top-10">
                                                  <div class="star-rating" title="Rated 4.50 out of 5">
                                                      <span style="width: 90%">4.50 out of 5</span>
                                                  </div>
                                              </div>
                                            </div>
                                        </div>
                                  </div>
                                  <div class="item padding_5">
                                      <div class="medium_article_list">
                                          <div class="thumb_wrapper" >
                                                  <div class="type_box">
                                                      <div class="type">食</div>
                                                  </div>
                                                <a href="#" class="" title="">
                                                    <img src="demo/banners/demo750x450-5.jpg" alt="">
                                                </a>
                                          </div>
                                            <div class="content_wrapper padding_5">
                                                <h5><a href="#">在地聯盟店家名稱</a></h5>
                                                <p class="text_grey">高雄市 博愛特區</p>
                                                <div class="rate_type margin-top-10">
                                                  <div class="star-rating" title="Rated 4.50 out of 5">
                                                      <span style="width: 90%">4.50 out of 5</span>
                                                  </div>
                                              </div>
                                            </div>
                                        </div>
                                  </div>
                                  <div class="item padding_5">
                                      <div class="medium_article_list">
                                          <div class="thumb_wrapper">
                                                  <div class="type_box">
                                                      <div class="type">食</div>
                                                  </div>
                                                <a href="#" class="" title="">
                                                    <img src="demo/banners/demo750x450-6.jpg" alt="">
                                                </a>
                                          </div>
                                            <div class="content_wrapper padding_5">
                                                <h5><a href="#">在地聯盟店家名稱</a></h5>
                                                <p class="text_grey">高雄市 博愛特區</p>
                                                <div class="rate_type margin-top-10">
                                                  <div class="star-rating" title="Rated 4.50 out of 5">
                                                      <span style="width: 90%">4.50 out of 5</span>
                                                  </div>
                                              </div>
                                            </div>
                                        </div>
                                  </div>
                                  <div class="item padding_5">
                                      <div class="medium_article_list">
                                          <div class="thumb_wrapper">
                                                  <div class="type_box">
                                                      <div class="type">食</div>
                                                  </div>
                                                <a href="#" class="" title="">
                                                    <img src="demo/banners/demo750x450-4.jpg" alt="">
                                                </a>
                                          </div>
                                            <div class="content_wrapper padding_5">
                                                <h5><a href="#">在地聯盟店家名稱</a></h5>
                                                <p class="text_grey">高雄市 博愛特區</p>
                                                <div class="rate_type margin-top-10">
                                                  <div class="star-rating" title="Rated 4.50 out of 5">
                                                      <span style="width: 90%">4.50 out of 5</span>
                                                  </div>
                                              </div>
                                            </div>
                                        </div>
                                  </div>
                                  <div class="item padding_5">
                                      <div class="medium_article_list">
                                          <div class="thumb_wrapper" >
                                                  <div class="type_box">
                                                      <div class="type">食</div>
                                                  </div>
                                                <a href="#" class="" title="">
                                                    <img src="demo/banners/demo750x450-5.jpg" alt="">
                                                </a>
                                          </div>
                                            <div class="content_wrapper padding_5">
                                                <h5><a href="#">在地聯盟店家名稱</a></h5>
                                                <p class="text_grey">高雄市 博愛特區</p>
                                                <div class="rate_type margin-top-10">
                                                  <div class="star-rating" title="Rated 4.50 out of 5">
                                                      <span style="width: 90%">4.50 out of 5</span>
                                                  </div>
                                              </div>
                                            </div>
                                        </div>
                                  </div>
                                  <div class="item padding_5">
                                      <div class="medium_article_list">
                                          <div class="thumb_wrapper" >
                                                  <div class="type_box">
                                                      <div class="type">食</div>
                                                  </div>
                                                <a href="#" class="" title="">
                                                    <img src="demo/banners/demo750x450-6.jpg" alt="">
                                                </a>
                                          </div>
                                            <div class="content_wrapper padding_5">
                                                <h5><a href="#">在地聯盟店家名稱</a></h5>
                                                <p class="text_grey">高雄市 博愛特區</p>
                                                <div class="rate_type margin-top-10">
                                                  <div class="star-rating" title="Rated 4.50 out of 5">
                                                      <span style="width: 90%">4.50 out of 5</span>
                                                  </div>
                                              </div>
                                            </div>
                                        </div>
                                  </div>
                                  <div class="item padding_5">
                                      <div class="medium_article_list">
                                          <div class="thumb_wrapper" >
                                                  <div class="type_box">
                                                      <div class="type">食</div>
                                                  </div>
                                                <a href="#" class="" title="">
                                                    <img src="demo/banners/demo750x450-4.jpg" alt="">
                                                </a>
                                          </div>
                                            <div class="content_wrapper padding_5">
                                                <h5><a href="#">在地聯盟店家名稱</a></h5>
                                                <p class="text_grey">高雄市 博愛特區</p>
                                                <div class="rate_type margin-top-10">
                                                  <div class="star-rating" title="Rated 4.50 out of 5">
                                                      <span style="width: 90%">4.50 out of 5</span>
                                                  </div>
                                              </div>
                                            </div>
                                        </div>
                                  </div>
                              </div>
                                              <!-- End OWL Slider -->
                                  
                                  </div>
                                  <div class="z_bluebk z_lineheigh30 z_fontcwhile z_padding10 z_margintop30">
                                      雲端護照任務
                                  </div>
                                  
                                  
                                  <?php for($b=0;$b<1;$b++){?>
                                  <div class="row z_margintop30">
                                  	<div class="col-sm-4"><img src="img-main/postcard03.jpg" width="100%"></div>
                                    <div class="col-sm-8">
                                    	<div class="z_titleh5"><a href="#">跟著五月天到新年．塔吉特千層蛋糕專賣店。</a></div>
                                        
                                        <div class="h6 z_contentfont">老屋改建的店舖亂有感觸，就像「老屋町」一樣，老有老的好，雖不見得每個人...都喜歡這種新</div>
                                        
                                    </div>
                                  </div>
                                  <div class="clearfix"></div>
                                  <?php }?>
                                  <div class="z_margintop10 z_border_cc">
                               	    	<img src="img-main/banner728x90.jpg" class="visible-lg visible-sm visible-md"> 
                                        <img src="demo/banners/320X100.jpg" class="visible-xs">
                                    </div>
                                  
                                  
                        </div>
                        <!-- ======== SIDEBAR ======== -->
                        <div class="col col_4_of_12 sidebar sb_right300 xs_hide">
                        
                           <?php include("right1213button.php");?>
                        </div>
                    </div>
                </div>
            </section>
            <!-- ======== FOOTER ======== -->
            <?php include("footer.php");?>

</div>
        </div>
        
        <!--leftmenu-->
    <div class="sb-slidebar sb-left sb-style-overlay">
        <!-- Main Navigation -->
        <?php include("left_menu.php");?>
        <!-- /Main Navigation -->
    </div>
        <!--righttmenu-->
		<div class="sb-slidebar sb-right sb-style-overlay">
          <div class="col col_12_of_12 sidebar">
          		<?php include("right1213button.php");?>   
          </div>
        </div>
        
        
        
        <!-- ======== JAVASCRIPTS ======== -->
        <script src="js/jquery.min.js"></script>
        <script src="js/jquery.ui.min.js"></script>
        <script src="js/jquery.fitvids.js"></script>
        <script src="js/jquery.owlcarousel.min.js"></script>
        <script src="js/jquery.magnific.popup.min.js"></script>
        <script src="js/jquery.parallax.min.js"></script>
        <script src="js/jquery.smooth.scroll.js"></script>
        <script src="js/jquery.init.js"></script>
         <script src="sys_plugin/oki_zippicker/oki_zipcode_mem.js"></script>
  <!-- MyWeather JS (needed) -->
  <script src="3dParty/MyWeather/js/MyWeather.js"></script>
  <script src="3dParty/bootstrap/js/bootstrap.minzone.js"></script>
       
        
<!-- owl-carousel -->
    <script>
    $(document).ready(function() {
      $("#owl-demo").owlCarousel({
          navigation : false, // Show next and prev buttons
          slideSpeed : 300,
          paginationSpeed : 400,
          singleItem:true,
		  autoPlay : true
     
          // "singleItem:true" is a shortcut for:
          // items : 1, 
          // itemsDesktop : false,
          // itemsDesktopSmall : false,
          // itemsTablet: false,
          // itemsMobile : false
      });
    });
	$('#t_owl_slider').owlCarousel({
          navigation : false, // Show next and prev buttons
          slideSpeed : 300,
          paginationSpeed : 400,
          singleItem:true,
		  autoPlay : true,
})

	$('.shop_owl_slider').owlCarousel({
          navigation : true, // Show next and prev buttons
          pagination : false,
          slideSpeed : 300,
          paginationSpeed : 400,
          autoPlay : true,
loop:true,
    margin:10,
    items : 3, 
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        468:{
            items:2,
            nav:false
        },
        989:{
            items:3,
            nav:true,
            loop:false
        }
    }
})
	$('.postcard_owl_slider').owlCarousel({
          navigation : true, // Show next and prev buttons
          pagination : false,
          slideSpeed : 300,
          paginationSpeed : 400,
          autoPlay : true,
loop:true,
    margin:10,
    items : 3, 
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        468:{
            items:2,
            nav:false
        },
        989:{
            items:3,
            nav:true,
            loop:false
        }
    }
})
    </script>
    
<!-- Slidebars -->
<script src="3dParty/slidebars/slidebars.min.js"></script>
<script>
    (function($) {
        $(document).ready(function() {
            $.slidebars();
      
      $("#schbtn").on("click",function(){ $("#schform").submit() });
      
        });
    }) (jQuery);
</script>

<!-- weather -->
<script type="text/javascript">

 $.MyWeather({
           elementid: "#localinfo",
           position: "right",
           showpopup: true,
           showinfo: true,
           temperature: "c",
           closeicon: false,
           showicon: true,
           showtemperature: true,
           showlocation: true,
           showip: false,
           size: 80,
           iconcolor: "#ffffff",
           fontcolor: "#ffffff",
},
function(City,Country, IP, Latitude, Longitude, Temperature){
// This callback function is called when we get those values. The callback is optional too.
}); 


</script> 

    </body>
</html>
<script>