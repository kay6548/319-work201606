<!DOCTYPE html>
<html  lang="zh-Hant-TW">
    <head>
        <title>319鄉鎮</title>
        <!-- ======== META TAGS ======== -->
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <!-- ======== FAVICONS ======== -->
        <link rel="icon" href="favicon.ico">
        <link rel="apple-touch-icon" href="favicon.png">
        <!-- ======== STYLESHEETS ======== -->
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/typography.css">
        <link rel="stylesheet" href="css/fontawesome.css">
        <link rel="stylesheet" href="css/popup.css">
        <link rel="stylesheet" href="css/owlslider.css">
        <link rel="stylesheet" href="css/style.css">
        <link id="switch_style" rel="stylesheet" href="demo/main-color/blue.css">
        <!-- Slidebars CSS -->
        <link rel="stylesheet" href="3dParty/slidebars/slidebars.min.css">
        <!-- ======== RESPONSIVE ======== -->
        <link rel="stylesheet" media="(max-width:479px)" href="css/responsive-small.css">
        <link rel="stylesheet" media="(min-width:480px) and (max-width:768px)" href="css/responsive-0.css">
        <link rel="stylesheet" media="(min-width:769px) and (max-width:992px)" href="css/responsive-768.css">
        <link rel="stylesheet" media="(min-width:993px) and (max-width:1200px)" href="css/responsive-992.css">
        <link rel="stylesheet" media="(min-width:1201px)" href="css/responsive-1200.css">
        
        <!-- Slidebars CSS -->
        <link rel="stylesheet" href="3dParty/owl-carousel/owl.carousel.css">
        <link rel="stylesheet" href="3dParty/owl-carousel/owl.theme.css">

        <!-- MyWeather CSS (needed) -->
        <link rel="stylesheet" type="text/css" href="3dParty/MyWeather/css/MyWeather.css">
        <link rel="stylesheet" type="text/css" href="3dParty/bootstrap/css/zbootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="3dParty/bootstrap/css/bootstrap-theme.min.css">
        <link rel="stylesheet" type="text/css" href="css/zonestyle.css">



        <!-- ======== GOOGLE FONTS ======== -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Titillium+Web:400,300,300italic,400italic,700,700italic,600italic,600">
        <link href='https://fonts.googleapis.com/css?family=PT+Sans:400italic,400|Raleway:200italic,300,300italic|Oxygen:300' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Ubuntu:400,300,300italic' rel='stylesheet' type='text/css'>
        <!-- ======== GOOGLE ANALYTICS ======== -->
    <style>
    .owl-carousel{ display:block !important}

    #owl-demo .item img{
        display: block;
        width: 100%;
        height: auto;
    }


    </style>
    </head>
    <body>
    <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/zh_TW/sdk.js#xfbml=1&version=v2.6";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
        
        <!-- ======== WRAPPER ======== -->
        <div id="wrapper " class="wide">
        <div id="sb-site">
            <!-- ======== BANNER ======== -->
            <div class="top-banner728x90 text_center aligncenter bg_grey">
				<script type='text/javascript'>
                
                  var googletag = googletag || {};
                
                  googletag.cmd = googletag.cmd || [];
                
                  (function() {
                
                    var gads = document.createElement('script');
                
                    gads.async = true;
                
                    gads.type = 'text/javascript';
                
                    var useSSL = 'https:' == document.location.protocol;
                
                    gads.src = (useSSL ? 'https:' : 'http:') +
                
                      '//www.googletagservices.com/tag/js/gpt.js';
                
                    var node = document.getElementsByTagName('script')[0];
                
                    node.parentNode.insertBefore(gads, node);
                
                  })();
                
                </script>
                <script type='text/javascript'>
                
                  googletag.cmd.push(function() {
                
                    googletag.defineSlot('/47573522/travel_down_728x90', [728, 90], 'div-gpt-ad-1447638964347-0').addService(googletag.pubads());
                
                    googletag.pubads().enableSingleRequest();
                
                    googletag.enableServices();
                
                  });
                
                </script>            
                <a href="#" target="_blank">
                <!-- /47573522/travel_down_728x90 -->
                
                <div id='div-gpt-ad-1447638964347-0' style='height:90px; width:728px; margin:0 auto;'>
                
                <script type='text/javascript'>
                
                googletag.cmd.push(function() { googletag.display('div-gpt-ad-1447638964347-0'); });
                
                </script>
                
                </div>
                </a>
            </div>
            <!-- ======== HEADER ======== -->
            <?php include("header_menu.php");?>
            
            <!-- ======== Slider CONTAINER ======== -->
            <div class="fullwidth bg_grey">
                <div class="container">
                    <ul class="breadcrumb">
                      <li><a href="#">首頁</a></li>
                      <li><a href="#">單元名稱</a></li>
                      <li>頁面名稱</li>
                    </ul> 
                </div> 
            </div>
            
            
            <!-- ======== SECTION ======== -->
            <section id="page_wrapper">
                <div class="container">
                    <div class="row">
                        <!-- ======== MAIN CONTENT ======== -->
                        <div class="col col_12_of_12 main_content">
                                  
                                  <div class="col col-sm-2 col-xs-6 z_fontright z_cardlr">
                                  	<div class="car_nextbtn"><a href="#" style="margin-top:200px;">〈 上一則</a></div>
                                  </div>
                                  <div class="col col-sm-2 col-xs-6 hidden-lg hidden-md hidden-sm z_cardlr">
                                  <a href="#">下一則 〉</a>
                                  </div>
                                  <div class="col col-sm-8 col-xs-12">
                                  			<div class="z_bluebk z_lineheigh30 z_fontcwhile z_padding10">
                                               標題文字
                                            </div>
                                            <div class="z_margintop10">
                                                  <div class="z_border_cc" style="margin:5px;">
                                                  			<div class="" style="margin:0 auto; margin:20px;">
                                                       	    <img src="img-main/postcard01.jpg" width="100%"> 
                                                            </div>
                                                            <div style="border-top:1px solid #ccc; height:1px; margin:20px;"></div>
                                                            <div class="clearfix"></div>
                                                            <div style=" background:url(img-main/postbk.jpg); background-repeat:no-repeat;height:50px;"></div>
                                                 
                                                  </div>
                                                  
                                            </div>
                                            <div class="z_margintop30 z_contentfont">
                                            春夏，草山步道有花海守候，秋冬，這兒有北投溫泉。大屯山腳下的盆地裡，裝盛著
全球華人世界最自由的文化。創意市集、表演藝術、街頭遊行日日上演。剝皮寮是電
影『艋舺』裡的最佳主角。故宮在歷史中激盪當代創意，夜市與『牛肉麵節』讓這個
城市令人垂涎。春夏，草山步道有花海守候，秋冬，這兒有北投溫泉。春夏，草山步
道有花海守候，秋冬，這兒有北投溫泉。大屯山腳下的盆地裡，裝盛著
全球華人世界最自由的文化。創意市集、表演藝術、街頭遊行日日上演。剝皮寮是電
影『艋舺』裡的最佳主角。故宮在歷史中激盪當代創意，夜市與『牛肉麵節』讓這個
城市令人垂涎。春夏，草山步道有花海守候，秋冬，這兒有北投溫泉。
                                            </div>
                                            <div style="border-top:1px solid #ccc; height:1px; margin:20px 0px;">
                                            
                                            </div>
                                            <div class="z_titlebtn6-3">
                                                        	<a href="#"><img src="img-main/images/download_03.jpg"></a>
                                                            <a href="#"><img src="img-main/images/download_05.png"></a>
                                            </div>
                                             <div class="clearfix visible-xs"></div>
                                            <div class="z_margintop10 z_contentfont">
                                                <p> 分享人：小吳</p>
                                                <p>分享地點：台北顯示  大安地</p>
                                                <p>標籤：大安區區</p>
                                            </div>
                                            <div class="z_margintop10">
                                            </div>
                                            <div class="z_border_cc z_margintop10">
                                            	<img src="img-main/banner728x90.jpg" /> 
                                            </div>
                                            <div class="z_border_cc z_margintop10">
                                            	<div class="fb-comments" data-href="https://developers.facebook.com/docs/plugins/comments#configurator" data-width="100%" data-numposts="1"></div>
                                            </div>
                                  
                                  </div>
                                  <div class="col col-sm-2 col-xs-2 hidden-xs z_cardlr">
                                  <div class="car_nextbtn"><a href="#">下一則 〉</a></div>
                                  </div>
                                  
                                  <div class="clearfix"></div>
                                  <div class="col col-sm-12">
                                  		<div class="z_margintop10">
                                    			<div class="widget widget_search ">
                                                    <div class="widget_title text_center">
                                                    <img src="img-main/3-2.jpg" alt=""/>
                                                    </div>
                                                </div>
                                                <!-- OWL Slider -->
                                                <div id="" class="postcard_owl_slider2 item_slider owl-carousel owl-theme">                                                   <?php for($i=0;$i<5;$i++){?>
                                                    <div class="item padding_5">
                                                        <div class="medium_article_list">
                                                             <div class="thumb_wrapper" >
                                                                  <a href="#" class="" title="">
                                                                      <img src="demo/banners/demo750x450-7.jpg" alt="">
                                                                  </a>
                                                              </div>
                                                              <div class="row">
                                                                  <div class="col col-sm-2 col-xs-2">
                                                                  		<div class="z_radius50"></div>	
                                                                  </div>
                                                                  <div class="col col-sm-3 col-xs-5 z_fontright z_fontptop10">幕後黑手</div>
                                                                  <div class="col col-sm-7 col-xs-5 z_fontright z_fontptop10">2015/06/04</div>
                                                              </div>
                                                              <div class="clearfix"></div>
                                                              <div class="h5 z_titleh5"><a href="#">跟著五月天到新年．塔吉特千層蛋糕專賣店。</a></div>
                                                              <div class="h6 z_contentfont">老屋改建的店舖亂有感觸，就像「老屋町」一樣，老有老的好，雖不見得每個人...都喜歡這種新</div>

                                                          </div> 
                                                    </div>
                                                    <?php }?>
                                                </div>
                                            <!-- End OWL Slider -->
                                    		
                                       </div>
                                       <div class="z_margintop10">
                                    			<div class="widget widget_search ">
                                                    <div class="widget_title text_center">
                                                    <img src="img-main/8-2.jpg" alt=""/>
                                                    </div>
                                                </div>
                                                <!-- OWL Slider -->
                                                <div id="" class="postcard_owl_slider item_slider owl-carousel owl-theme">
                                                    <div class="item padding_5">
                                                        <div class="medium_article_list">
                                                            <div class="thumb_wrapper" >
                                                                  <a href="#" class="" title="">
                                                                      <img src="demo/banners/demo750x450-7.jpg" alt="">
                                                                  </a>
                                                            </div>
                                                              <div class="content_wrapper padding_5">
                                                                  <h5><a href="#">旅行明信片標題旅行明信片標題旅行明信片標題</a></h5>
                                                                  <div>
                                                                  <i class="fa fa-map-marker"></i>
                                                                  <span class="text_grey">高雄市 博愛特區</span>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                    </div>
                                                    <div class="item padding_5">
                                                        <div class="medium_article_list">
                                                            <div class="thumb_wrapper" >
                                                                  <a href="#" class="" title="">
                                                                      <img src="demo/banners/demo750x450-8.jpg" alt="">
                                                                  </a>
                                                            </div>
                                                              <div class="content_wrapper padding_5">
                                                                  <h5><a href="#">旅行明信片標題旅行明信片標題旅行明信片標題</a></h5>
                                                                  <div>
                                                                  <i class="fa fa-map-marker"></i>
                                                                  <span class="text_grey">高雄市 博愛特區</span>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                    </div>
                                                    <div class="item padding_5">
                                                        <div class="medium_article_list">
                                                            <div class="thumb_wrapper" >
                                                                  <a href="#" class="" title="">
                                                                      <img src="demo/banners/demo750x450-9.jpg" alt="">
                                                                  </a>
                                                            </div>
                                                              <div class="content_wrapper padding_5">
                                                                  <h5><a href="#">旅行明信片標題旅行明信片標題旅行明信片標題</a></h5>
                                                                  <div>
                                                                  <i class="fa fa-map-marker"></i>
                                                                  <span class="text_grey">高雄市 博愛特區</span>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                    </div>
                                                    <div class="item padding_5">
                                                        <div class="medium_article_list">
                                                            <div class="thumb_wrapper">
                                                                  <a href="#" class="" title="">
                                                                      <img src="demo/banners/demo750x450-7.jpg" alt="">
                                                                  </a>
                                                            </div>
                                                              <div class="content_wrapper padding_5">
                                                                  <h5><a href="#">旅行明信片標題旅行明信片標題旅行明信片標題</a></h5>
                                                                  <div>
                                                                  <i class="fa fa-map-marker"></i>
                                                                  <span class="text_grey">高雄市 博愛特區</span>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                    </div>
                                                    <div class="item padding_5">
                                                        <div class="medium_article_list">
                                                            <div class="thumb_wrapper">
                                                                  <a href="#" class="" title="">
                                                                      <img src="demo/banners/demo750x450-8.jpg" alt="">
                                                                  </a>
                                                            </div>
                                                              <div class="content_wrapper padding_5">
                                                                  <h5><a href="#">旅行明信片標題旅行明信片標題旅行明信片標題</a></h5>
                                                                  <div>
                                                                  <i class="fa fa-map-marker"></i>
                                                                  <span class="text_grey">高雄市 博愛特區</span>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                    </div>
                                                    <div class="item padding_5">
                                                        <div class="medium_article_list">
                                                            <div class="thumb_wrapper">
                                                                  <a href="#" class="" title="">
                                                                      <img src="demo/banners/demo750x450-9.jpg" alt="">
                                                                  </a>
                                                            </div>
                                                              <div class="content_wrapper padding_5">
                                                                  <h5><a href="#">旅行明信片標題旅行明信片標題旅行明信片標題</a></h5>
                                                                  <div>
                                                                  <i class="fa fa-map-marker"></i>
                                                                  <span class="text_grey">高雄市 博愛特區</span>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                    </div>
                                                    <div class="item padding_5">
                                                        <div class="medium_article_list">
                                                            <div class="thumb_wrapper" >
                                                                  <a href="#" class="" title="">
                                                                      <img src="demo/banners/demo750x450-7.jpg" alt="">
                                                                  </a>
                                                            </div>
                                                              <div class="content_wrapper padding_5">
                                                                  <h5><a href="#">旅行明信片標題旅行明信片標題旅行明信片標題</a></h5>
                                                                  <div>
                                                                  <i class="fa fa-map-marker"></i>
                                                                  <span class="text_grey">高雄市 博愛特區</span>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                    </div>
                                                </div>
                                            <!-- End OWL Slider -->
                                    		
                                    </div>
                                        
                                        
                                  </div>
                                  
                                  
                                  
                        </div>
                        <!-- ======== 12of12 ======== -->
                        
                        
                    </div>
                </div>
                <!-- ======== container ======== -->
            </section>
            <!-- ======== FOOTER ======== -->
            <?php include("footer.php");?>

</div>
        </div>
        
        <!--leftmenu-->
    <div class="sb-slidebar sb-left sb-style-overlay">
        <!-- Main Navigation -->
        <?php include("left_menu.php");?>
        <!-- /Main Navigation -->
    </div>
     <!--righttmenu-->
		<div class="sb-slidebar sb-right sb-style-overlay">
          <div class="col col_12_of_12 sidebar">
          		<?php include("right1button.php");?>                    
          </div>
        </div>
        
        
        
        <!-- ======== JAVASCRIPTS ======== -->
        <script src="js/jquery.min.js"></script>
        <script src="js/jquery.ui.min.js"></script>
        <script src="js/jquery.fitvids.js"></script>
        <script src="js/jquery.owlcarousel.min.js"></script>
        <script src="js/jquery.magnific.popup.min.js"></script>
        <script src="js/jquery.parallax.min.js"></script>
        <script src="js/jquery.smooth.scroll.js"></script>
        <script src="js/jquery.init.js"></script>
         <script src="sys_plugin/oki_zippicker/oki_zipcode_mem.js"></script>
  <!-- MyWeather JS (needed) -->
  <script src="3dParty/MyWeather/js/MyWeather.js"></script>
  <script src="3dParty/bootstrap/js/bootstrap.minzone.js"></script>
       
        
<!-- owl-carousel -->
    <script>
    $(document).ready(function() {
      $("#owl-demo").owlCarousel({
          navigation : false, // Show next and prev buttons
          slideSpeed : 300,
          paginationSpeed : 400,
          singleItem:true,
      autoPlay : true
     
          // "singleItem:true" is a shortcut for:
          // items : 1, 
          // itemsDesktop : false,
          // itemsDesktopSmall : false,
          // itemsTablet: false,
          // itemsMobile : false
      });
	  $('#myTab a').click(function(e) {
                e.preventDefault()
                $(this).tab('show')
            })
    });
  $('#t_owl_slider').owlCarousel({
          navigation : false, // Show next and prev buttons
          slideSpeed : 300,
          paginationSpeed : 400,
          singleItem:true,
      autoPlay : true,
})

  $('.shop_owl_slider').owlCarousel({
          navigation : true, // Show next and prev buttons
          pagination : false,
          slideSpeed : 300,
          paginationSpeed : 400,
          autoPlay : true,
loop:true,
    margin:10,
    items : 3, 
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        468:{
            items:2,
            nav:false
        },
        989:{
            items:3,
            nav:true,
            loop:false
        }
    }
})
  $('.postcard_owl_slider').owlCarousel({
          navigation : true, // Show next and prev buttons
          pagination : false,
          slideSpeed : 300,
          paginationSpeed : 400,
          autoPlay : true,
loop:true,
    margin:10,
    items : 2, 
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        468:{
            items:2,
            nav:false
        },
        989:{
            items:2,
            nav:true,
            loop:false
        }
    }
})
$('.postcard_owl_slider2').owlCarousel({
          navigation : true, // Show next and prev buttons
          pagination : false,
          slideSpeed : 300,
          paginationSpeed : 400,
          autoPlay : true,
loop:true,
    margin:10,
    items : 3, 
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        468:{
            items:2,
            nav:false
        },
        989:{
            items:2,
            nav:true,
            loop:false
        }
    }
})
    </script>
    
<!-- Slidebars -->
<script src="3dParty/slidebars/slidebars.min.js"></script>
<script>
    (function($) {
        $(document).ready(function() {
            $.slidebars();
      
      $("#schbtn").on("click",function(){ $("#schform").submit() });
      
        });
    }) (jQuery);
</script>

<!-- weather -->
<script type="text/javascript">

 $.MyWeather({
           elementid: "#localinfo",
           position: "right",
           showpopup: true,
           showinfo: true,
           temperature: "c",
           closeicon: false,
           showicon: true,
           showtemperature: true,
           showlocation: true,
           showip: false,
           size: 80,
           iconcolor: "#ffffff",
           fontcolor: "#ffffff",
},
function(City,Country, IP, Latitude, Longitude, Temperature){
// This callback function is called when we get those values. The callback is optional too.
}); 


</script> 

    </body>
</html>
<script>