<!DOCTYPE html>
<html  lang="zh-Hant-TW">
    <head>
        <title>319鄉鎮</title>
        <!-- ======== META TAGS ======== -->
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <!-- ======== FAVICONS ======== -->
        <link rel="icon" href="favicon.ico">
        <link rel="apple-touch-icon" href="favicon.png">
        <!-- ======== STYLESHEETS ======== -->
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/typography.css">
        <link rel="stylesheet" href="css/fontawesome.css">
        <link rel="stylesheet" href="css/popup.css">
        <link rel="stylesheet" href="css/owlslider.css">
        <link rel="stylesheet" href="css/style.css">
        <link id="switch_style" rel="stylesheet" href="demo/main-color/blue.css">
        <!-- Slidebars CSS -->
        <link rel="stylesheet" href="3dParty/slidebars/slidebars.min.css">
        <!-- ======== RESPONSIVE ======== -->
        <link rel="stylesheet" media="(max-width:479px)" href="css/responsive-small.css">
        <link rel="stylesheet" media="(min-width:480px) and (max-width:768px)" href="css/responsive-0.css">
        <link rel="stylesheet" media="(min-width:769px) and (max-width:992px)" href="css/responsive-768.css">
        <link rel="stylesheet" media="(min-width:993px) and (max-width:1200px)" href="css/responsive-992.css">
        <link rel="stylesheet" media="(min-width:1201px)" href="css/responsive-1200.css">
        
        <!-- Slidebars CSS -->
        <link rel="stylesheet" href="3dParty/owl-carousel/owl.carousel.css">
        <link rel="stylesheet" href="3dParty/owl-carousel/owl.theme.css">

        <!-- MyWeather CSS (needed) -->
        <link rel="stylesheet" type="text/css" href="3dParty/MyWeather/css/MyWeather.css">
        <link rel="stylesheet" type="text/css" href="3dParty/bootstrap/css/zbootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="3dParty/bootstrap/css/bootstrap-theme.min.css">
        <link type="text/css" rel="stylesheet" href="demo/css/application.css">
        <link rel="stylesheet" type="text/css" href="css/zonestyle.css">



        <!-- ======== GOOGLE FONTS ======== -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Titillium+Web:400,300,300italic,400italic,700,700italic,600italic,600">
        <link href='https://fonts.googleapis.com/css?family=PT+Sans:400italic,400|Raleway:200italic,300,300italic|Oxygen:300' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Ubuntu:400,300,300italic' rel='stylesheet' type='text/css'>
        <!-- ======== GOOGLE ANALYTICS ======== -->
    <style>
    .owl-carousel{ display:block !important}

    #owl-demo .item img{
        display: block;
        width: 100%;
        height: auto;
    }


    </style>
    </head>
    <body>
        
        <!-- ======== WRAPPER ======== -->
        <div id="wrapper " class="wide">
        <div id="sb-site">
            <!-- ======== BANNER ======== -->
            <div class="top-banner728x90 text_center aligncenter bg_grey">
				<script type='text/javascript'>
                
                  var googletag = googletag || {};
                
                  googletag.cmd = googletag.cmd || [];
                
                  (function() {
                
                    var gads = document.createElement('script');
                
                    gads.async = true;
                
                    gads.type = 'text/javascript';
                
                    var useSSL = 'https:' == document.location.protocol;
                
                    gads.src = (useSSL ? 'https:' : 'http:') +
                
                      '//www.googletagservices.com/tag/js/gpt.js';
                
                    var node = document.getElementsByTagName('script')[0];
                
                    node.parentNode.insertBefore(gads, node);
                
                  })();
                
                </script>
                <script type='text/javascript'>
                
                  googletag.cmd.push(function() {
                
                    googletag.defineSlot('/47573522/travel_down_728x90', [728, 90], 'div-gpt-ad-1447638964347-0').addService(googletag.pubads());
                
                    googletag.pubads().enableSingleRequest();
                
                    googletag.enableServices();
                
                  });
                
                </script>            
                <a href="#" target="_blank">
                <!-- /47573522/travel_down_728x90 -->
                
                <div id='div-gpt-ad-1447638964347-0' style='height:90px; width:728px; margin:0 auto;'>
                
                <script type='text/javascript'>
                
                googletag.cmd.push(function() { googletag.display('div-gpt-ad-1447638964347-0'); });
                
                </script>
                
                </div>
                </a>
            </div>
            <!-- ======== HEADER ======== -->
            <?php include("header_menu.php");?>
            
            <!-- ======== Slider CONTAINER ======== -->
            <div class="fullwidth bg_grey">
                <div class="container">
                    <ul class="breadcrumb">
                      <li><a href="#">首頁</a></li>
                      <li><a href="#">單元名稱</a></li>
                      <li>頁面名稱</li>
                    </ul> 
                </div> 
            </div>
            
            
            <!-- ======== SECTION ======== -->
            <section id="page_wrapper">
                <div class="container">
                    <div class="row">
                        <!-- ======== MAIN CONTENT ======== -->
                        <div class="col col_8_of_12 main_content">
                                  <article class="single_post hasborder">
                                <!-- ======== ARTICLE CONTENT ==== -->
                                
                                <div class="col col_12_of_12">
                                      <div class="post_content">
                                           <div class="title_hot border-bottom padding_bottom_10 margin-bottom-10 padding_top_20">
                                            集點兌換
                                           </div>
                                           <form action="" method="get">
                                               <div class="row">                                           		
                                                 <div class="col col_3_of_12">
                                                    <a class="btn btn_large btn_bluenow" href="#">兌換記錄</a>
                                                 </div>
                                                 <div class="col col_9_of_12">
                                                    <h4 class="margin-top-10">商品：登山鞋男士特別版</h4>
                                                 </div>                                        
                                               </div>
                                               <div class="col_12_of_12">
                                               			<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                                          <!-- Indicators -->
                                                          <ol class="carousel-indicators">
                                                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                                            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                                            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                                          </ol>
                                                        
                                                          <!-- Wrapper for slides -->
                                                          <div class="carousel-inner" role="listbox">
                                                            <div class="item active">
                                                              <img src="img-travel/postcard02.jpg" alt="...">
                                                              
                                                            </div>
                                                            <div class="item">
                                                              <img src="img-travel/postcard03.jpg" alt="...">
                                                             
                                                            </div>
                                                            <div class="item">
                                                              <img src="img-travel/post-demo.jpg" alt="...">
                                                             
                                                            </div>
                                                         
                                                          </div>
                                                        
                                                          <!-- Controls -->
                                                          <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                                            <span class="sr-only">Previous</span>
                                                          </a>
                                                          <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                                            <span class="sr-only">Next</span>
                                                          </a>
                                                        </div>
                                               
                                               </div><!--結 -->
                                               <div class="row margin-top-20">
                                               		<div class="col col_6_of_12">
                                                    			<p>市價：4500元</p>
                                                                <p>結束日期：2016/10/10</p>
                                                                <p>每人最多兌換次數：2次</p>
                                                                <p>需要點數：1000點</p>
                                                                <p>抽獎機會：總共累計20次</p>
                                                    </div>
                                                    <div class="col col_6_of_12">
                                                    		<p class="border-bottom">產品規格</p>
                                                            <p>男士 M號 L號</p>
                                                    </div>
                                               </div> 
                                               <div class="title_hot border-bottom padding_bottom_10 margin-bottom-10 padding_top_20">
                                                相關注意事項
                                               </div>
                                               <div class="col_12_of_12">
                                               			<p>◎HYPERgrip耐磨橡膠大底，耐磨性強、抓地力佳 
                                                          <br>
                                                          ◎BOA美國專利鞋帶綁定系統，快捷方便且堅固耐用 
                                                          <br>
                                                          ◎專利NesTFIT技術，不只提高支撐加強防滑性，更貼合腳踝的自然輪廓 
                                                          <br>
                                                          ◎輕量好走 
◎GORE-TEXR防水透氣結構</p>
                                               </div>
                                                <div class="col col_12_of_12 margin-top-20 margin-bottom-20" style="text-align:center;">
                                                        
                                                         <button type="button" class="btn z_bluebk" data-dismiss="modal">頁面返回</button>
                                                </div>
                                            <!--結 -->
                                            </form>            
                                      </div>                                	
                                </div>
                                <div class="clearfix"></div>
                            </article>
                        </div>
                        <!-- ======== SIDEBAR ======== -->
                        <div class="col col_4_of_12 sidebar sb_right300 xs_hide">
                        
                            <!-- ======== NEWS ======== -->
                            <div class="breaking_news light border-bottom">
                              <div class="breaking_block">
                                <div class="item">
                                    <div class="icon"></div>
                                    <span class="text_grey">[2015/09/18]</span><br>
                                    最佳借問站站長 票選活動開跑。
                                    快來投下你心目中最親切、最熱
                                    情、最有特色的站長一票吧!!!...
                                </div>
                                <div class="item">
                                    <div class="icon"></div>
                                    <span class="text_grey">[2015/09/18]</span><br>
                                    最佳借問站站長 票選活動開跑。
                                    快來投下你心目中最親切、最熱
                                    情、最有特色的站長一票吧!!!...
                                </div>
                                <div class="item">
                                    <div class="icon"></div>
                                    <span class="text_grey">[2015/09/18]</span><br>
                                    最佳借問站站長 票選活動開跑。
                                    快來投下你心目中最親切、最熱
                                    情、最有特色的站長一票吧!!!...
                                </div>
                              </div>
                            </div>
                            
                            
                            <!-- ======== WIDGET - BANNER 300X250 ======== -->
                            <div class="widget widget_banner_300x600">
                                <div class="banner">
                                <img src="demo/banners/300x600.jpg" alt=""/> 
                                </div>
                            </div>
                            <!-- ======== WIDGET - 特別企劃 300X100 ======== -->
                            <div class="widget banner300x100">
                                <div class="widget_title no-border-bottom">
                                <img src="img-main/title-special.png" alt=""/>
                                </div>
                                <div class="banner">
                                    <a href="http://smiletaiwan.cw.com.tw/12summer" target="_blank"><img src="img-main/sp_01.jpg" alt=""></a>
                                    <p>追日。追風。追海的旅行</p>
                                </div>
                                <div class="banner">
                                    <a href="http://smiletaiwan.cw.com.tw/matsufun" target="_blank"><img src="img-main/sp_02.jpg" alt=""></a>
                                    <p>杖起舵兒往前滑！馬祖，等你</p>
                                </div>
                                <div class="banner">
                                    <a href="http://smiletaiwan.cw.com.tw/native" target="_blank"><img src="img-main/sp_03.jpg" alt=""></a>
                                    <p>傾聽部落。你有東西留在我這</p>
                                </div>
                            </div>
                        
                            <!-- ======== WIDGET - BANNER 300X250 ======== -->
                            <div class="widget widget_banner_300x250">
                                <div class="banner">
								<script type='text/javascript'>
                                
                                  var googletag = googletag || {};
                                
                                  googletag.cmd = googletag.cmd || [];
                                
                                  (function() {
                                
                                    var gads = document.createElement('script');
                                
                                    gads.async = true;
                                
                                    gads.type = 'text/javascript';
                                
                                    var useSSL = 'https:' == document.location.protocol;
                                
                                    gads.src = (useSSL ? 'https:' : 'http:') +
                                
                                      '//www.googletagservices.com/tag/js/gpt.js';
                                
                                    var node = document.getElementsByTagName('script')[0];
                                
                                    node.parentNode.insertBefore(gads, node);
                                
                                  })();
                                
                                </script>
                                <script type='text/javascript'>
                                
                                  googletag.cmd.push(function() {
                                
                                    googletag.defineSlot('/47573522/article_right_300x250(u)', [300, 250], 'div-gpt-ad-1447638783144-0').addService(googletag.pubads());
                                
                                    googletag.pubads().enableSingleRequest();
                                
                                    googletag.enableServices();
                                
                                  });
                                
                                </script>                                
                                <a href="#" target="_blank">
                                <!-- /47573522/article_right_300x250(u) -->
                                <div id='div-gpt-ad-1447638783144-0' style='height:250px; width:300px;'>
                                
                                <script type='text/javascript'>
                                
                                googletag.cmd.push(function() { googletag.display('div-gpt-ad-1447638783144-0'); });
                                
                                </script>
                                
                                </div>
                                </a>
                                </div>
                            </div>
                            <!-- ======== WIDGET - BANNER 300X250 ======== -->
                            <div class="widget widget_banner_300x250">
                                <div class="banner">
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/zh_TW/sdk.js#xfbml=1&version=v2.5&appId=196008494115242";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div class="fb-page" data-href="https://www.facebook.com/smiletaiwan319" data-tabs="timeline" data-width="300" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/smiletaiwan319"><a href="https://www.facebook.com/smiletaiwan319">微笑台灣319鄉+</a></blockquote></div></div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- ======== FOOTER ======== -->
            <?php include("footer.php");?>

</div>
        </div>
        
        <!--leftmenu-->
    <div class="sb-slidebar sb-left sb-style-overlay">
        <!-- Main Navigation -->
        <?php include("left_menu.php");?>
        <!-- /Main Navigation -->
    </div>
        <!--righttmenu-->
		<div class="sb-slidebar sb-right sb-style-overlay">
          <div class="col col_12_of_12 sidebar">
                              <!-- ======== WIDGET - BANNER 300X100 ======== -->
                              <div class="widget banner300x100 margin-bottom-30">
                                  <div class="widget_title">
                                  <img src="img-main/title-special.png" alt=""/>
                                  </div>
                                <div class="banner margin-bottom-30">
                                    <a href="http://smiletaiwan.cw.com.tw/12summer" target="_blank"><img src="img-main/sp_01.jpg" alt=""></a>
                                    <p>追日。追風。追海的旅行</p>
                                </div>
                                <div class="banner margin-bottom-30">
                                    <a href="http://smiletaiwan.cw.com.tw/matsufun" target="_blank"><img src="img-main/sp_02.jpg" alt=""></a>
                                    <p>杖起舵兒往前滑！馬祖，等你</p>
                                </div>
                                <div class="banner margin-bottom-30">
                                    <a href="http://smiletaiwan.cw.com.tw/native" target="_blank"><img src="img-main/sp_03.jpg" alt=""></a>
                                    <p>傾聽部落。你有東西留在我這</p>
                                </div>
                              </div>
                          
                              <!-- ======== WIDGET - 駐站旅人 ======== -->
                              <div class="widget widget_socialize margin-bottom-20">
                                  <div class="widget_title no-border-bottom">
                                  <img src="img-main/title-traveler.png" alt=""/> 
                                  </div>
                                  <div class="row">
                                      <div class="col_4_of_12 alignleft">
                                      <div class="writer-single">
                                                      <img src="img-main/person_pic_default.jpg" alt="">
                                                      <p>作者名稱</p>
                                                  </div>
                                      </div>
                                      <div class="col_4_of_12 alignleft">
                                      <div class="writer-single">
                                                      <img src="img-main/person_pic_default.jpg" alt="">
                                                      <p>作者名稱</p>
                                                  </div>
                                      </div>
                                      <div class="col_4_of_12 alignleft">
                                      <div class="writer-single">
                                                      <img src="img-main/person_pic_default.jpg" alt="">
                                                      <p>作者名稱</p>
                                                  </div>
                                      </div>
                                      <div class="col_4_of_12 alignleft">
                                      <div class="writer-single">
                                                      <img src="img-main/person_pic_default.jpg" alt="">
                                                      <p>作者名稱</p>
                                                  </div>
                                      </div>
                                      <div class="col_4_of_12 alignleft">
                                      <div class="writer-single">
                                                      <img src="img-main/person_pic_default.jpg" alt="">
                                                      <p>作者名稱</p>
                                                  </div>
                                      </div>
                                      <div class="col_4_of_12 alignleft">
                                      <div class="writer-single">
                                                      <img src="img-main/person_pic_default.jpg" alt="">
                                                      <p>作者名稱</p>
                                                  </div>
                                      </div>
                                  </div>
                              </div>
                              <!-- ======== WIDGET - BANNER 300X250 ======== -->
                              <div class="widget widget_banner_300x250 margin-bottom-20">
                                  <div class="banner">
									<script type='text/javascript'>
                                    
                                      var googletag = googletag || {};
                                    
                                      googletag.cmd = googletag.cmd || [];
                                    
                                      (function() {
                                    
                                        var gads = document.createElement('script');
                                    
                                        gads.async = true;
                                    
                                        gads.type = 'text/javascript';
                                    
                                        var useSSL = 'https:' == document.location.protocol;
                                    
                                        gads.src = (useSSL ? 'https:' : 'http:') +
                                    
                                          '//www.googletagservices.com/tag/js/gpt.js';
                                    
                                        var node = document.getElementsByTagName('script')[0];
                                    
                                        node.parentNode.insertBefore(gads, node);
                                    
                                      })();
                                    
                                    </script>
                                    <script type='text/javascript'>
                                    
                                      googletag.cmd.push(function() {
                                    
                                        googletag.defineSlot('/47573522/article_right_300x250(u)', [300, 250], 'div-gpt-ad-1447638783144-0').addService(googletag.pubads());
                                    
                                        googletag.pubads().enableSingleRequest();
                                    
                                        googletag.enableServices();
                                    
                                      });
                                    
                                    </script>                                
                                    <a href="#" target="_blank">
                                    <!-- /47573522/article_right_300x250(u) -->
                                    <div id='div-gpt-ad-1447638783144-0' style='height:250px; width:300px;'>
                                    
                                    <script type='text/javascript'>
                                    
                                    googletag.cmd.push(function() { googletag.display('div-gpt-ad-1447638783144-0'); });
                                    
                                    </script>
                                    
                                    </div>
                                    </a>
                                  </div>
                              </div>
                              <!-- ======== WIDGET - LATEST POSTS ======== -->
                              <div class="widget widget_latest_posts margin-bottom-20">
                                  <div class="widget_title no-border-bottom">
                                  <img src="img-main/title_top.png" alt=""/> 
                                  </div>
                                  <div class="item">
                                      <div class="item_header">
                                          <i class="fa fa-3x fa-angle-right text_grey"></i>
                                      </div>
                                      <div class="item_wrapper">
                                          <div class="item_content">
                                            <h4><a #href="#" target="_blank">【新竹 旅遊】客家湯圓節健走。傳統技藝新玩法</a></h4>
                                             <p>哪哪麻這天來到竹東火車站參加湯圓節的活動, 話說竹東每年都會辦一次湯圓節, 有園遊會賣當地特色產品 ...</p>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="item">
                                      <div class="item_header">
                                          <i class="fa fa-3x fa-angle-right text_grey"></i>
                                      </div>
                                      <div class="item_wrapper">
                                          <div class="item_content">
                                            <h4><a #href="#" target="_blank">當老師問我……</a></h4>
                                             <p>當老師問我： 「耶誕節都怎樣慶祝？」當我的外籍老師問我時，直覺地脫口而出，「這個啊，我們都過農曆年的。」 老師很有興致，...</p>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="item">
                                      <div class="item_header">
                                          <i class="fa fa-3x fa-angle-right text_grey"></i>
                                      </div>
                                      <div class="item_wrapper">
                                          <div class="item_content">
                                            <h4><a #href="#" target="_blank">台中大雪山．步道散步吸收芬多精 & 雪山神木 & 天</a></h4>
                                             <p>炎熱的夏天，是不是很想上山消暑呢？鳥夫人：不是~~~~ 呵呵，因為鳥夫人喜歡大海不喜歡山，但鳥先生跟鳥夫人相反，所以兩個人...</p>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                              <!-- ======== WIDGET - BANNER 300X250 ======== -->
                              <div class="widget widget_banner_300x250">
                                  <div class="banner margin-bottom-30">
								  <script type='text/javascript'>
                                  
                                    var googletag = googletag || {};
                                  
                                    googletag.cmd = googletag.cmd || [];
                                  
                                    (function() {
                                  
                                      var gads = document.createElement('script');
                                  
                                      gads.async = true;
                                  
                                      gads.type = 'text/javascript';
                                  
                                      var useSSL = 'https:' == document.location.protocol;
                                  
                                      gads.src = (useSSL ? 'https:' : 'http:') +
                                  
                                        '//www.googletagservices.com/tag/js/gpt.js';
                                  
                                      var node = document.getElementsByTagName('script')[0];
                                  
                                      node.parentNode.insertBefore(gads, node);
                                  
                                    })();
                                  
                                  </script>
                                  <script type='text/javascript'>
                                  
                                    googletag.cmd.push(function() {
                                  
                                      googletag.defineSlot('/47573522/article_right_300x250(d)', [300, 250], 'div-gpt-ad-1447638832673-0').addService(googletag.pubads());
                                  
                                      googletag.pubads().enableSingleRequest();
                                  
                                      googletag.enableServices();
                                  
                                    });
                                  
                                  </script>
                                  <a href="#" target="_blank">
                                  <!-- /47573522/article_right_300x250(d) -->
                                  <div id='div-gpt-ad-1447638832673-0' style='height:250px; width:300px;'>
                                  
                                  <script type='text/javascript'>
                                  
                                  googletag.cmd.push(function() { googletag.display('div-gpt-ad-1447638832673-0'); });
                                  
                                  </script>
                                  
                                  </div>
                                  </a>
                                  </div>
                              </div>
                          </div>
        </div>
        
<div class="modal fade z_margintop30" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel" style="text-align:center;">幸福點數-點數抽獎</h4>
      </div>
      <div class="modal-body">
        <div class="z_fontc666 z_fontcenter">您確定要使用 330點數來兌換『木質明信片』
的抽獎機會嗎？點選「確定」後，系統會直接
從您的幸福點數中扣除需要的點數。</div>
          
            
        
      </div>
      <div class="modal-footer" style="text-align:center;">
        <button type="button" class="btn z_bluebk" data-dismiss="modal">確定</button>
        <button type="button" class="btn z_bluebk">取消</button>
      </div>
    </div>
  </div>
</div>  
        
        <!-- ======== JAVASCRIPTS ======== -->
        <script src="js/jquery.min.js"></script>
        <script src="js/jquery.ui.min.js"></script>
        <script src="js/jquery.fitvids.js"></script>
        <script src="js/jquery.owlcarousel.min.js"></script>
        <script src="js/jquery.magnific.popup.min.js"></script>
        <script src="js/jquery.parallax.min.js"></script>
        <script src="js/jquery.smooth.scroll.js"></script>
        <script src="js/jquery.init.js"></script>
         <script src="sys_plugin/oki_zippicker/oki_zipcode_mem.js"></script>
  <!-- MyWeather JS (needed) -->
  <script src="3dParty/MyWeather/js/MyWeather.js"></script>
  <script src="3dParty/bootstrap/js/bootstrap.minzone.js"></script>
  <script type="text/javascript" src="lib/jquery.raty.min.js"></script>
       
        
<!-- owl-carousel -->
    <script>
    $(document).ready(function() {
	  $.fn.raty.defaults.path = 'lib/img';
      $('#size-demo1').raty({size:24,width:100});
	  $('#size-demo2').raty({size:24,width:100});
	  $('#size-demo3').raty({size:24,width:100});
	  $('#size-demo4').raty({size:24,width:100});
	  $('#size-demo5').raty({size:24,width:100});
      $("#owl-demo").owlCarousel({
          navigation : false, // Show next and prev buttons
          slideSpeed : 300,
          paginationSpeed : 400,
          singleItem:true,
		  autoPlay : true
     
          // "singleItem:true" is a shortcut for:
          // items : 1, 
          // itemsDesktop : false,
          // itemsDesktopSmall : false,
          // itemsTablet: false,
          // itemsMobile : false
      });
	  $('#myModal').modal('show');
    });
	$('#t_owl_slider').owlCarousel({
          navigation : false, // Show next and prev buttons
          slideSpeed : 300,
          paginationSpeed : 400,
          singleItem:true,
		  autoPlay : true,
})

	$('.shop_owl_slider').owlCarousel({
          navigation : true, // Show next and prev buttons
          pagination : false,
          slideSpeed : 300,
          paginationSpeed : 400,
          autoPlay : true,
loop:true,
    margin:10,
    items : 3, 
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        468:{
            items:2,
            nav:false
        },
        989:{
            items:3,
            nav:true,
            loop:false
        }
    }
})
	$('.postcard_owl_slider').owlCarousel({
          navigation : true, // Show next and prev buttons
          pagination : false,
          slideSpeed : 300,
          paginationSpeed : 400,
          autoPlay : true,
loop:true,
    margin:10,
    items : 3, 
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        468:{
            items:2,
            nav:false
        },
        989:{
            items:3,
            nav:true,
            loop:false
        }
    }
})
    </script>
    
<!-- Slidebars -->
<script src="3dParty/slidebars/slidebars.min.js"></script>
<script>
    (function($) {
        $(document).ready(function() {
            $.slidebars();
      
      $("#schbtn").on("click",function(){ $("#schform").submit() });
      
        });
    }) (jQuery);
</script>

<!-- weather -->
<script type="text/javascript">

 $.MyWeather({
           elementid: "#localinfo",
           position: "right",
           showpopup: true,
           showinfo: true,
           temperature: "c",
           closeicon: false,
           showicon: true,
           showtemperature: true,
           showlocation: true,
           showip: false,
           size: 80,
           iconcolor: "#ffffff",
           fontcolor: "#ffffff",
},
function(City,Country, IP, Latitude, Longitude, Temperature){
// This callback function is called when we get those values. The callback is optional too.
}); 


</script> 

    </body>
</html>
<script>